# Istio

Hier werden die lokalen Istio-Sourcen gebaut und deployed

# Build docker images

```bash
./make.sh
```
Damit wird der Operator, pilot und proxyv2 gebaut, und in das Starterregistry monitoring.slainte.at:5000 hochgeladen.

# Achtung - Das funktioniert nicht

Es ist recht schwierig, ISTIO-Projekte zu finden, die am Raspberry-PI funktionieren. Zum Zeitpunkt der Erstellung, war die nur
docker.io/querycapistio/. Großer Dank an den Entwickler.

Ich habe noch ein paar lokale Adaptionen dazugefügt. Prinzipeill kommt ISTI hoch, es ist mir aber nicht gelungen Services sinnvoll zu deployen. Darum habe ich diese Versuche eingestellt (und warte auf ein offiziell supportetes ISTIO-Feature in microK8s)-
