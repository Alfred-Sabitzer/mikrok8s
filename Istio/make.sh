#!/bin/bash
############################################################################################
# Bauen und deployen
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
git pull
cd ./operator
./do.sh
cd ..
cd ./pilot
./do.sh
cd ..
cd ./proxyv2
./do.sh
cd ..
#

