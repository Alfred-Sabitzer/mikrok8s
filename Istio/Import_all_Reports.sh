#!/bin/bash
############################################################################################
#
# MicroK8S Importieren aller Grafana Dashboards in ISTIO
#
# https://grafana.com/docs/grafana/latest/http_api/dashboard/#create-update-dashboard
# https://github.com/monitoringartist/grafana-utils
#
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#
cmd="kubectl -n istio-system port-forward service/grafana 3000:3000"
${cmd} &
pid=$!
sleep 3s
FILES="./General/*.json
"
#
for filename in $FILES ; do
  if [ -f "${filename}" ]
  then
    echo ${filename}
    cat ${filename} | jq '. * {overwrite: true, dashboard: {id: null}}' | curl -L -X POST -H "Content-Type: application/json" http://localhost:3000/api/dashboards/db -d @-
  fi
done
#
kill ${pid}
#
# Jetzt sind alle Dashboards verfügbar
#
