#!/bin/bash
############################################################################################
# Bauen und deployen
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
git pull

docker_registry="monitoring.slainte.at:5000"
docker_username=$(cat ${HOME}/.password/docker_basis_username.txt)
docker_password=$(cat ${HOME}/.password/docker_basis_password.txt)
git pull
image="querycapistio/operator"
tag="1.14.1"
docker build --no-cache --force-rm . -t ${docker_registry}/${image}:${tag} -t ${docker_registry}/${image}:latest
docker push ${docker_registry}/${image}:${tag}
docker push ${docker_registry}/${image}:latest
#curl -u ${docker_username}:${docker_password} -k -v https://${docker_registry}/v2/_catalog           # Check ob man was zurückbekommt
curl -u ${docker_username}:${docker_password} -k -s https://${docker_registry}/v2/${image}/tags/list
#

