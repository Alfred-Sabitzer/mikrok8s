echo "############################################################################################"
echo "#"
echo "# Testen der Redis-Verbindung - funktioniert über den default-User "
echo "#"
echo "############################################################################################"
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
export REDIS_DB=0
source ./redis.bash
redis_connect -h redis.slainte.svc.cluster.local -a ${REDIS_AUTH}
redis_exec PING
redis_exec SET hostname $(hostname -f)
redis_exec INFO keyspace
redis_exec 'keys *'
redis_exec GET hostname
redis_exec ACL LIST
redis_exec ACL WHOAMI
redis_disconnect