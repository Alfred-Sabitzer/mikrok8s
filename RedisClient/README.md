# Redis Client

Unter Verwendung der Sourcen von https://github.com/SomajitDey/redis-client wrid eine Verbindung zum Redis-Cluster hergestellt. Dies funktioniert wie bei vielen anderen Redis-Clients (wie z.b. NextCloud) ohne TLS und über den Default-Account.
# Bauen des Image (Ubuntu)

```bash
./do.sh
```
Damit wird das kokrete Image erzeugt, und im K8S-Docker-Repository abgelegt.
# Testen

Verbinden zum Pod

```bash
./kexex.sh
```
# Durchführen des Tests

Ausführen des vorbereiteten Test-Skripts

```bash
./test.sh
```