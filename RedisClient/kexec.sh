#!/bin/sh
# Connect in den Redisclient Container
mypod=$(kubectl get pod -n slainte | grep -i redisclient | awk '{print $1 }')
kubectl exec -i -t -n slainte ${mypod} -c redisclient "--" sh -c "clear; (bash || ash || sh)"
