#!/bin/sh
#############################################################################################"
#"
# Installation Mailu"
#"
#############################################################################################"
kubectl create namespace mailu-mailserver
helm repo add mailu https://mailu.github.io/helm-charts/
helm repo update
helm show values mailu/mailu > values-file.yaml
helm install mailu mailu/mailu -n mailu-mailserver --values values-file.yaml
helm uninstall mailu --namespace=mailu-mailserver
kubectl delete namespace mailu-mailserver
#
