#!/bin/sh
# Disable exit on non 0
set +ex
echo "############################################################################################"
echo "#"
echo "# Starten Mailu Admin container mit den richtigen Werten."
echo "#"
echo "############################################################################################"
#
echo "# Administrator-Password passend zum Keypass setzen"
flask mailu password ${INITIAL_ADMIN_ACCOUNT} ${DOMAIN} ${INITIAL_ADMIN_PW}
echo "# Applikation starten"
python3 /start.py
echo "# Sicherstellen, dass der Prozess oben bleibt"
/dummy.sh
#