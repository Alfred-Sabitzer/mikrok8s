#!/bin/sh
# Disable exit on non 0
set +e
echo "############################################################################################"
echo "#"
echo "# Starten Postfix mit den richtigen Werten."
echo "#"
echo "############################################################################################"
#
# Sicherstellen, dass der Prozess oben bleibt
python3 /start.py
/dummy.sh
#