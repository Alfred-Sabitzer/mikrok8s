#!/bin/bash
############################################################################################
# Alles Bauen und deployen
############################################################################################
shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
cd ./busybox
./do.sh
cd ../Hello-World
./do.sh
cd ../NextCloud/docker
./do.sh
cd ../../Postgres/docker
./do.sh
cd ../../Redis
./do.sh
cd ../RedisClient/docker
./do.sh
cd ../../RedisInsight-K8S
./do.sh
cd ../Registry-reorg
./do.sh
cd ../Joomla/docker
./do.sh
#
# Nun sollten alle Images an ihrem Platz sein
#
