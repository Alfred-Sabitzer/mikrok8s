#!/bin/sh
############################################################################################
# Reorganisieren der Registry docker.k8s.slainte.at
############################################################################################
echo "$*"
date
ENV_REORG=${1}
echo "Purge-Value:"${ENV_REORG}
if [ "${ENV_REORG} " = " " ] ;
 then
   ENV_REORG="3"
fi

docker_username=$(cat /passwords/username)
docker_password=$(cat /passwords/password)
image="registry-reorg"
cluster_service="registryk8s-svc"

# Prüfen, ob die docker-registry bereit ist
/check_ready.sh ${image} ${cluster_service}
ret_code=$?
if [[ $ret_code -ne 0 ]] ;
then
  echo "${cluster_service} ist nicht bereit"
  exit 1
fi

echo "${cluster_service} ist bereit"
date
docker_registry="$(cat /${cluster_service}.CRES):5000"
echo "Docker-Registry: ${docker_registry}"
date
curl -k -v https://${docker_registry}

# Anzeige der Repository-Einträge
repositories=$(curl -k -u ${docker_username}:${docker_password} -s https://${docker_registry}/v2/_catalog)
echo "Repositories: ${repositories}"
for repo in $(echo "${repositories}" | jq -r '.repositories[]'); do
  echo ${repo}
  tags=$(curl -k -u ${docker_username}:${docker_password} -s "https://${docker_registry}/v2/${repo}/tags/list" | jq -r '.tags[]')
  for tag in ${tags}; do
    echo "____"${tag}
  done
done
#
echo "Alle Repository-Einträge angezeigt"
date
#
for repo in $(echo "${repositories}" | jq -r '.repositories[]'); do
  echo ${repo}
  rm -f /tmp/${repo}.txt
  touch /tmp/${repo}.txt
  tags=$(curl -k -u ${docker_username}:${docker_password} -s "https://${docker_registry}/v2/${repo}/tags/list" | jq -r '.tags[]')
  for tag in ${tags}; do
	  if [ "${tag} " != "latest " ] ;
	  then
      echo ${tag} >> /tmp/${repo}.txt
    fi
  done
# Die ältesten löschen (bis auf 3)
  sort -f -r /tmp/${repo}.txt > /tmp/${repo}.sort
  tail -n +${ENV_REORG}  /tmp/${repo}.sort > /tmp/${repo}.tail
  sort -f  /tmp/${repo}.tail > /tmp/${repo}.delete
# Löschen der Einträge
  tags=$(cat /tmp/${repo}.delete)
  echo "Tags: ${tags}"

  for tag in ${tags}; do
    echo "Lösche      "${repo}:${tag}
    curl -v -k -X DELETE "https://${docker_registry}/v2/${repo}/manifests/$(
          curl -k -s -I \
              -H "Accept: application/vnd.docker.distribution.manifest.v2+json" \
              "https://${docker_registry}/v2/${repo}/manifests/${tag}" \
          | awk '$1 == "docker-content-digest:" { print $2 }' \
          | tr -d $'\r' \
        )"
  done
done
date
echo " Anzeige der verbleibenden Einträge"
echo ${repositories}
for repo in $(echo "${repositories}" | jq -r '.repositories[]'); do
  echo ${repo}
  tags=$(curl -k -u ${docker_username}:${docker_password} -s "https://${docker_registry}/v2/${repo}/tags/list" | jq -r '.tags[]')
  for tag in ${tags}; do
    echo "____"${tag}
  done
done
date
echo "---Fertig---"
# Istio ist ein wenig seltsam:)
#curl -fsI -X POST http://localhost:15020/quitquitquit