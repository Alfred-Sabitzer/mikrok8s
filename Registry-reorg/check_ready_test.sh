#!/bin/sh
############################################################################################
# Check ob bereit (über das API). Dazu ist ein Service-Account notwendig
############################################################################################
image="registry-reorg"
cluster_service="registryk8s-svc"
export K8S=https://$KUBERNETES_SERVICE_HOST:$KUBERNETES_SERVICE_PORT
export TOKEN=$(cat /var/run/secrets/tokens/${image}-token/token)
export CACERT=/var/run/secrets/tokens/${image}-token/ca.crt
export NAMESPACE=$(cat /var/run/secrets/tokens/${image}-token/namespace)
export CRES=""
rm -f ${0}.CRES
export

CRES=$(curl -s -w "%{http_code}" -H "Authorization: Bearer ${TOKEN}" --cacert ${CACERT} ${K8S}/api/v1/namespaces/admin/services/${cluster_service})
echo "ClusterRES: ${CRES} "
CRES=$(curl -s -w "%{http_code}" -H "Authorization: Bearer ${TOKEN}" --cacert ${CACERT} ${K8S}/api/v1/namespaces/admin/services/${cluster_service} | grep -i '"clusterIP": ')
echo "ClusterRES: ${CRES} "
pf="\"clusterIP\": \""
sf="\","
xtext=${CRES#*"$pf"}
xtext=${xtext%"$sf"*}
echo "Cluster IP: ${xtext}"
echo "${CRES}" > "/${cluster_service}.CRES"
