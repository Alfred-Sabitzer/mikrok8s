# Registry Reorg

Reorganisieren der microk8s Registry. Dies funktioniert mit einem Kubernetes-Cron-Job.

## Ablauf
Es werden alle Einträge bis auf die letzten 3 gepurged.
Das kann mit dem Parameter ENV_REORG gesteuert werden.

## Support
Es gibt keinen Support für diese Software. Jede Software hat experimentellen Charakter. Es wird dringend davon abgeraten, diese Software produktiv zu verwenden.

## Lizenz
GPL https://www.gnu.org/licenses/gpl-3.0.html
