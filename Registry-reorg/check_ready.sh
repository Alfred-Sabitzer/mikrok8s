#!/bin/sh
############################################################################################
# Check ob bereit (über das API). Dazu ist ein Service-Account notwendig
############################################################################################
image="${1}"
cluster_service="${2}"
export K8S=https://$KUBERNETES_SERVICE_HOST:$KUBERNETES_SERVICE_PORT
export TOKEN=$(cat /var/run/secrets/tokens/${image}-token/token)
export CACERT=/var/run/secrets/tokens/${image}-token/ca.crt
export NAMESPACE=$(cat /var/run/secrets/tokens/${image}-token/namespace)
export CRES=""
rm -f ${0}.CRES
#export

i=0
while [[ $i -lt 5 ]]
do
  echo "try: $i"
  i=$((i+1))
  CRES=$(curl -s -w "%{http_code}" -H "Authorization: Bearer ${TOKEN}" --cacert ${CACERT} ${K8S}/api/v1/namespaces/admin/services/${cluster_service} | grep -i '"clusterIP": ')
  echo "ClusterRES: ${CRES} "
  if [[ ! "${CRES} "  ==  " " ]]; then
    pf="\"clusterIP\": \""
    sf="\","
    xtext=${CRES#*"$pf"}
    xtext=${xtext%"$sf"*}
    echo "Cluster IP: ${xtext}"
    export CRES=${xtext}
    echo "${CRES}" > "/${cluster_service}.CRES"
    exit 0  # Alles gut
  fi
  sleep 5
done
# Fehlerfall
exit 1
