# Redis Image

Unter Verwendung des Basis-Images wird hier ein konkretes Redis-Image gebaut. Hier werden auch die richtigen Verzeichnisse gesetzt. Die Inhalte kommen dann später aus der Kubernetes-Config.

# Installation

```bash
./do.sh
```
Damit wird das kokrete Image erzeugt, und im K8S-Docker-Repository abgelegt.
