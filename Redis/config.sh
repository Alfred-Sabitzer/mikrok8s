#!/bin/sh
echo "############################################################################################"
echo "#"
echo "# Adaption der Configuration"
echo "#"
echo "############################################################################################"

export REDISCLI_AUTH=$(cat /tmp/redissecret/password)
export REDIS_USER=$(cat /tmp/redissecret/username)
export REDIS_HOSTNAME=$(cat /etc/hostname)
export REDIS_HOSTNAME_FULL=$(hostname  -f)

if [ "${REDIS_USE_TLS}x" == "YESx" ]; then
  echo "TLS-Configuration for Standard-Port"
  export REDIS_TLS=" --tls --cert /etc/ssl/certs/client.crt --key /etc/ssl/certs/client.key --cacert /etc/ssl/certs/ca.crt "
else
  echo "No TLS-Configuration"
  export REDIS_TLS=" "
fi

ls -lisaR /etc/redis

echo "MASTER_FDQN ${MASTER_FDQN} auth"
#redis-cli -h ${MASTER_FDQN} -p 6379 ${REDIS_TLS} auth ${REDIS_USER} ${REDISCLI_AUTH}

RESPONSE=$(cat <<EOF | redis-cli -h ${MASTER_FDQN} -p 6379 ${REDIS_TLS} | tail -n 1
           auth ${REDIS_USER} ${REDISCLI_AUTH}
           PING
EOF
)

echo "RESPONSE ${RESPONSE}"
if [ "${RESPONSE}" != "PONG" ]; then
  echo "master not found, defaulting to redis-0"
fi

echo "hostname $(hostname)"

if [ "$(hostname)" == "redis-0" ]; then
  echo "this is redis-0, not updating config..."
else
  echo "updating redis.conf..."
  echo "slaveof ${MASTER_FDQN} 6379" >> /etc/redis/redis.conf
fi
#
echo "changing masterauth and masteruser"
sed -i 's,# masterauth <master-password>,masterauth '${REDISCLI_AUTH}',g' /etc/redis/redis.conf
sed -i 's,# masteruser <username>,masteruser '${REDIS_USER}',g' /etc/redis/redis.conf
sed -i 's,# requirepass foobared,requirepass '${REDISCLI_AUTH}',g' /etc/redis/redis.conf
echo "user ${REDIS_USER} ~* +@all on >${REDISCLI_AUTH}" >> /etc/redis/redis.conf
# echo "user default off -@all on >${REDISCLI_AUTH}" >> /etc/redis/redis.conf
echo "user default ~* +@all on >${REDISCLI_AUTH}" >> /etc/redis/redis.conf
echo "changing TLS-Usage"
echo "${REDIS_USE_TLS}x"

if [ "${REDIS_USE_TLS}x" == "YESx" ]; then
  echo "TLS-Configuration for Standard-Port"
  sed -i 's,# TLS_USAGE,tls-port 6379 ,g' /etc/redis/redis.conf
else
  echo "No TLS-Configuration"
  sed -i 's,# TLS_USAGE,port 6379 ,g' /etc/redis/redis.conf
  sed -i 's,tls-,# tls-,g' /etc/redis/redis.conf
fi