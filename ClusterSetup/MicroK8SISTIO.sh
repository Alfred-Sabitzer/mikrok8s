#!/bin/bash
############################################################################################
#
# MicroK8S ISTIO Service Mesh
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
indir=$(dirname "$0")
echo "Wird noch nicht supported!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
exit
# ISTIO Disablen
ansible pc1 -m shell -a 'microk8s.disable istio'
# ISTIO Enablen
ansible pc1 -m shell -a 'echo "Y" | microk8s.enable istio'
#
${indir}/check_running_pods.sh
#
# Jetzt haben wir einen Servicemesh
#