#!/bin/bash
############################################################################################
#
# Setzen dre benötigten Environment-Variablen
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
# Environmentvariable - Wird für Python3 gebraucht https://click.palletsprojects.com/en/5.x/python3/
sudo sed --in-place '/LC_ALL=/d' /etc/environment
echo 'LC_ALL=C.UTF-8' >> /etc/environment
sed --in-place '/LANG=/d' /etc/environment
echo 'LANG=C.UTF-8' >> /etc/environment
#
# Environmentvariablen sind gesetzt
#