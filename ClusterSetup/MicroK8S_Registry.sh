#!/bin/bash
############################################################################################
#
# Einrichten einer eigenen Registry.
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
indir=$(dirname "$0")
workdir=$(pwd)
#
${indir}/check_running_pods.sh
#
cd ../Registry
ansible pc1 -m shell -a 'microk8s disable registry '
./create_dockerconfigjson.sh
kubectl apply -f .
cd ${workdir}
${indir}/check_running_pods.sh
#
cd ../Registry-reorg
kubectl apply -f .
cd ${workdir}
${indir}/check_running_pods.sh
#
# Jetzt funktioniert die Secure Registry mit UI
#


