#!/bin/bash
############################################################################################
#
# MicroK8S Einrichten des CertManagers
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
indir=$(dirname "$0")
#
# CertManager Einrichten
kubectl create namespace cert-manager
ansible pc1 -m shell -a 'microk8s.helm3 repo add jetstack https://charts.jetstack.io  '  # Repo einspielen
ansible pc1 -m shell -a 'microk8s.helm3 repo update '
ansible pc1 -m shell -a 'microk8s helm3 uninstall cert-manager jetstack/cert-manager '
ansible pc1 -m shell -a 'microk8s helm3 install cert-manager jetstack/cert-manager \
  --namespace cert-manager --version v1.9.1 \
  --set installCRDs=true \
  --set ingressShim.defaultIssuerName=letsencrypt-production \
  --set ingressShim.defaultIssuerKind=ClusterIssuer \
  --set ingressShim.defaultIssuerGroup=cert-manager.io '
kubectl apply -f ${indir}/cert-manager.yaml
#
${indir}/check_running_pods.sh
#
# Jetzt können wir Zertifikate generieren
#
exit
# ab 1.25
# https://microk8s.io/docs/addon-cert-manager
ansible pc1 -m shell -a 'microk8s disable cert-manager '        # (core) Cloud native certificate management
kubectl delete -f ${indir}/cert-manager.yaml
#ansible pc -m shell -a 'microk8s status --wait-ready'
ansible pc1 -m shell -a 'microk8s enable cert-manager '         # (core) Cloud native certificate management
${indir}/check_running_pods.sh
#
kubectl apply -f ${indir}/cert-manager.yaml
rc=$?
echo "Returncode: ${rc}"
while  [ ${rc} -gt 0 ]
do
  sleep 30s
  kubectl apply -f ${indir}/cert-manager.yaml
  rc=$?
  echo "Returncode: ${rc}"
done
ansible pc -m shell -a 'microk8s status --wait-ready'
