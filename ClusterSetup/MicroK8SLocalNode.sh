#!/bin/bash
############################################################################################
#
# Initialisierung auf dem lokalen Node
# Auf jedem Node einloggen, und Setup durchführen.
# Das ist eine notwendige Vorbereitung bevor der MicroK8S-Cluster aufgesetzt werden kann.
#
# wget https://gitlab.com/Alfred-Sabitzer/mikrok8s/-/raw/main/ClusterSetup/MicroK8SLocalNode.sh
#
############################################################################################
IFS="
"
#
if [[ !	${1} =~	(pc1|pc2|pc3|pc4|pc5) ]]; then
  echo "Usage ${0} (pc1|pc2|pc3|pc4|pc5) Passwort"
  exit 1
fi
Node=${1}
if [[ "${2} " == " " ]]; then
  echo "Usage ${0} (pc1|pc2|pc3|pc4|pc5) Passwort"
  exit 1
fi
PASS=${2}
#
#shopt -o -s errexit #—Terminates  the shell script if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#
# Wir werden Root
#
iam=$(whoami)
if [[ ! "${iam} " == "root " ]]; then
  echo "Benutzer muß Root sein"
  exit 1
fi
#
# Standarduser Ubuntu updaten
#
echo ${PASS} > /tmp/pass.txt
echo ${PASS} >> /tmp/pass.txt
cat /tmp/pass.txt |  passwd ubuntu
# Sudo einrichten
sed --in-place '/ALL=(ALL:ALL) NOPASSWD:ALL/d' /etc/sudoers
echo '%sudo   ALL=(ALL:ALL) NOPASSWD:ALL'  >> /etc/sudoers
#
# Anlegen der User
#
username=$(cat /etc/passwd | grep -i alfred)
if [[ ! "${username} " == " " ]]; then
  userdel -f -r alfred
fi
username=$(cat /etc/passwd | grep -i ansible)
if [[ ! "${username} " == " " ]]; then
  userdel -f -r ansible
fi
useradd  --comment "Systemadmin" -m --shell "/bin/bash" --password $(openssl passwd $PASS)  alfred
useradd  --comment "Systembenutzer für ansible" -m --shell "/bin/bash" --password  $(openssl passwd $PASS)  ansible
usermod -aG sudo alfred
usermod -aG sudo ansible
#
# ssh-keys
#
mkdir -p /home/alfred/.ssh/
curl -L https://gitlab.com/Alfred-Sabitzer/mikrok8s/-/raw/main/ClusterSetup/alfred_id_rsa.pub -o /home/alfred/.ssh/authorized_keys
chown -R alfred:alfred /home/alfred/.ssh
chown -R alfred:alfred /home/alfred/.ssh/authorized_keys
mkdir -p /home/ansible/.ssh/
curl -L https://gitlab.com/Alfred-Sabitzer/mikrok8s/-/raw/main/ClusterSetup/ansible_id_rsa.pub -o /home/ansible/.ssh/authorized_keys
chown -R ansible:ansible /home/ansible/.ssh
chown -R ansible:ansible /home/ansible/.ssh/authorized_keys
#
# Firmware-Settings
#
# 22.04 LTS
rm -f /boot/firmware/cmdline.txt
curl -L https://gitlab.com/Alfred-Sabitzer/mikrok8s/-/raw/main/ClusterSetup/cmdline.txt -o /boot/firmware/cmdline.txt
#
# Hostname
#
echo ${Node} > /etc/hostname
#
# Hosts-File
#
curl -L https://gitlab.com/Alfred-Sabitzer/mikrok8s/-/raw/main/ClusterSetup/replace_host_id.sh -o /root/replace_host_id.sh
chmod 755 /root/replace_host_id.sh
/root/replace_host_id.sh
NODEIP=$(cat ./nodeip.txt)
echo ${NODEIP}
#
# Network-Config
#
curl -L https://gitlab.com/Alfred-Sabitzer/mikrok8s/-/raw/main/ClusterSetup/99-disable-network-config.cfg -o /etc/cloud/cloud.cfg.d/99-disable-network-config.cfg
curl -L https://gitlab.com/Alfred-Sabitzer/mikrok8s/-/raw/main/ClusterSetup/50-cloud-init.yaml -o /etc/cloud/cloud.cfg.d/50-cloud-init.yaml
sed -i "s,#NODE#,${NODEIP},g" /etc/cloud/cloud.cfg.d/50-cloud-init.yaml
#
# NTP
#
curl -L https://gitlab.com/Alfred-Sabitzer/mikrok8s/-/raw/main/ClusterSetup/timesyncd.conf -o /etc/systemd/timesyncd.conf
systemctl stop systemd-timesyncd.service
systemctl enable systemd-timesyncd.service
systemctl start systemd-timesyncd.service
systemctl status systemd-timesyncd.service
#
# Startup-file für Modprobe
#
curl -L https://gitlab.com/Alfred-Sabitzer/mikrok8s/-/raw/main/ClusterSetup/modprobe.service -o /etc/systemd/system/modprobe.service
chown root:root /etc/systemd/system/modprobe.service
chmod 755 /etc/systemd/system/modprobe.service
systemctl stop modprobe.service
systemctl enable modprobe.service
systemctl start modprobe.service
systemctl status modprobe.service | cat
#
# Lokale Rechte
#

#
# Jetzt ist der Node Startklar und kann mit ansible weiter konfiguriert werden
#