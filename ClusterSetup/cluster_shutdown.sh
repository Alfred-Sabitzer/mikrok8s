#!/bin/bash
# Fährt alle Nodes im Cluster herunter
ansible pc -m shell -a 'microk8s stop'
ansible pc -m shell -a 'microk8s status --wait-ready'
ansible pc -m shell -a 'sudo shutdown -h now'
