#!/bin/bash
############################################################################################
#
# Einrichten K8Tz
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
indir=$(dirname "$0")
workdir=$(pwd)
#
${indir}/check_running_pods.sh
#
cd ../K8TZ
./K8TZ.sh
#
cd ${workdir}
${indir}/check_running_pods.sh
#
# Jetzt haben alle die richtige Zeit
#


