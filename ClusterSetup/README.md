# ClusterSetup

Hier finden sich alle Files, die zum automatisierten Setup des Kubernetes Cluster notwendig sind.
Dieser Kubernetes Cluster besteht aus mehreren Raspberry-PI Rechnern.

## Vorbereitung

Es bedarf korrekt aufgesetzter Raspberry-PI Knoten. Diese Knoten sind auf Ubuntu-Basis.

## Startskript

Das Skript, das alles startet und steuert ist ClusterSetup.sh

```
./ClusterSetup.sh
```

In diesem Script werden alle notwendigen Schritte in der richtigen Reihenfolge ausgeführt.
