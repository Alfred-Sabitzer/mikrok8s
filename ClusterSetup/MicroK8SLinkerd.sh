#!/bin/bash
############################################################################################
#
# MicroK8S Einrichten des Linkerd Service Mesh
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
indir=$(dirname "$0")
# Disablen
kubectl apply -f ./namespaces.yaml                           # Namespaces für Linkerd enablen
ansible pc1 -m shell -a 'microk8s disable linkerd'           # Disablen des Service-Mesh
${indir}/check_running_pods.sh
${indir}/cluster_reboot.sh
# Enable
${indir}/check_running_pods.sh
ansible pc1 -m shell -a 'microk8s enable linkerd'             # Enablen des Service-Mesh
${indir}/check_running_pods.sh
ansible pc1 -m shell -a 'microk8s linkerd check'              # Überprüfen der Funktionalität
kubectl apply -f ./linkerd.yaml                               # Ingress für den Zugang zum Linker-web
${indir}/check_running_pods.sh
${indir}/cluster_reboot.sh
${indir}/check_running_pods.sh
${indir}/MicroK8SGrafanaReports.sh                            # Nochmal die Reports einspielen und Passwort resetten. Offensichtlich wird die Datenbank überschrieben.
                                                              # in 1.24 wird ein zweiter prometheus und ein zweites Grafana installiert.
#