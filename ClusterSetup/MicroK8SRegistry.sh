#!/bin/bash
############################################################################################
#
# MicroK8S Einrichten des lokalen Registry für die Docker-Files
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
indir=$(dirname "$0")
# hosts initialisieren
ansible pc -m shell -a 'sudo /root/replace_host_id.sh '
# Registry Neu einrichten
ansible pc1 -m shell -a 'microk8s disable registry'
ansible pc -m shell -a 'microk8s status --wait-ready'
#
ansible pc1 -m shell -a 'microk8s enable registry:size=40Gi'
ansible pc -m shell -a 'microk8s status --wait-ready'
#
${indir}/check_running_pods.sh
#
# Patchen des Typs und der LoadBalancer-ID
kubectl apply -f ${indir}/registry-service.yaml
kubectl apply -f ${indir}/registry-config-map.yaml
# Patchen des PVC - OpenEBS can ReadWriteMany nicht :(
sc=$(kubectl get storageclass | grep -i openebs)
if [[ ! "${sc} " == " " ]]; then
  kubectl delete -f ${indir}/registry-claim.yaml
  kubectl apply -f ${indir}/registry-claim.yaml
fi
# Eintragen des Hosts in die Docker-Registry
ansible pc -m shell -a "sudo sed --in-place '/docker.registry:5000/d' /var/snap/microk8s/current/args/containerd-template.toml"
text='  [plugins."io.containerd.grpc.v1.cri".registry.mirrors."docker.registry:5000"]'
ansible pc -m shell -a "sudo echo  '${text}'  | sudo tee -a /var/snap/microk8s/current/args/containerd-template.toml "
text='    endpoint = ["http://docker.registry:5000"]'
ansible pc -m shell -a "sudo echo  '${text}'  | sudo tee -a /var/snap/microk8s/current/args/containerd-template.toml "
ansible pc -m shell -a "sudo tail /var/snap/microk8s/current/args/containerd-template.toml "
ansible pc -m shell -a "sudo snap restart microk8s.daemon-containerd"
#
# Am Build-Rechner (und allen anderen Rechnern, die docker builden, und die Registry verwenden wollen, muß dies richtig eingetragen werden.
#
ansible pc -m shell -a 'sudo mkdir -p /etc/docker'
ansible pc -m shell -a 'sudo curl -L https://gitlab.com/Alfred-Sabitzer/mikrok8s/-/raw/main/ClusterSetup/daemon.json -o /etc/docker/daemon.json'
ansible pc -m shell -a 'cat /etc/docker/daemon.json'
#
${indir}/check_running_pods.sh
#
# Jetzt ist die Registry verfügbar
#



