#!/bin/bash
# Updated den Cluster
ansible pc -m shell -a 'microk8s stop'
# Update kann zu seltsamen Effekten führen
ansible pc -m shell -a 'sudo snap info microk8s | grep -i tracking'
ansible pc -m shell -a 'sudo snap refresh microk8s --channel=latest/stable'
ansible pc -m shell -a 'sudo snap info microk8s | grep -i tracking'
# Update kann zu seltsamen Effekten führen
ansible pc -m shell -a 'sudo apt-get update && sudo apt-get upgrade -y'
ansible pc -m shell -a 'sudo /root/MicroK8S_Start.sh '
