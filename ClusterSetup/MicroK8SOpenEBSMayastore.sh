#!/bin/bash
############################################################################################
#
# MicroK8S Konfiguration von OpenEBS
#
# https://mayastor.gitbook.io/introduction/quickstart/preparing-the-cluster
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
indir=$(dirname "$0")
ansible-playbook -v ${indir}/check_hosts.yaml
# OpenEBS Disablen
ansible pc1 -m shell -a 'microk8s disable core/mayastor'
ansible pc1 -m shell -a 'kubectl delete namespace mayastor --grace-period=0 --force'
ansible pc -m shell -a 'microk8s status --wait-ready'
# Vorbereitung
ansible pc -m shell -a 'sudo sysctl vm.nr_hugepages=1024'
ansible pc -m shell -a 'sudo sed --in-place "/vm.nr_hugepages/d" /etc/sysctl.conf'
ansible pc -m shell -a 'echo "vm.nr_hugepages=1024" | sudo tee -a /etc/sysctl.conf'
ansible pc -m shell -a 'sudo apt install linux-modules-extra-$(uname -r)'
ansible pc -m shell -a 'sudo modprobe nvme_tcp'
ansible pc -m shell -a 'sudo sed --in-place "/nvme-tcp/d" /etc/modules-load.d/microk8s-mayastor.conf'
ansible pc -m shell -a 'echo "nvme-tcp" | sudo tee -a /etc/modules-load.d/microk8s-mayastor.conf'
# fstab initialisieren
ansible pc -m shell -a 'sudo curl -L https://gitlab.com/Alfred-Sabitzer/mikrok8s/-/raw/main/ClusterSetup/fstab -o /etc/fstab'
ansible pc -m shell -a 'cat /etc/fstab'
# Jetzt kommen die SSD's
ansible pc -m shell -a 'sudo umount -f /dev/sda'
ansible pc -m shell -a 'sudo wipefs -a /dev/sda'
ansible pc -m shell -a 'sudo mkfs.ext4 /dev/sda'
ansible pc -m shell -a 'sudo rm -f -R /var/data/openebs'
ansible pc -m shell -a 'sudo mkdir -p /var/data/openebs'
ansible pc -m shell -a 'sudo mount /dev/sda /var/data/openebs'
ansible pc -m shell -a 'df -h | grep -i sda'
# Hinzufügen der SSD-Disks
ansible pc -m shell -a 'sudo printf $(sudo blkid -o export /dev/sda | grep UUID)" /var/data/openebs       ext4    defaults        0       2" | sudo tee -a /etc/fstab'
ansible pc -m shell -a 'cat /etc/fstab'
#
ansible pc1 -m shell -a 'kubectl label node pc1 openebs.io/engine=mayastor'
ansible pc2 -m shell -a 'kubectl label node pc2 openebs.io/engine=mayastor'
ansible pc3 -m shell -a 'kubectl label node pc3 openebs.io/engine=mayastor'
ansible pc4 -m shell -a 'kubectl label node pc4 openebs.io/engine=mayastor'
ansible pc5 -m shell -a 'kubectl label node pc5 openebs.io/engine=mayastor'
ansible pc -m shell -a 'sudo shutdown -r now'
sleep 2m
ansible-playbook -v ${indir}/check_hosts.yaml
# Starten microk8s
ansible pc -m shell -a 'sudo /root/MicroK8S_Start.sh '
#
${indir}/check_running_pods.sh
# OpenEBS Enablen
ansible pc1 -m shell -a 'microk8s enable core/mayastor --default-pool-size 20G'
ansible pc -m shell -a 'microk8s status --wait-ready'
sleep 1m
# Kontroll-Statements
ansible pc -m shell -a 'df -h | grep -i sda'
ansible pc -m shell -a 'sudo grep HugePages /proc/meminfo'
ansible pc -m shell -a 'sudo cat /etc/sysctl.conf | grep -i huge'
ansible pc1 -m shell -a 'sudo microk8s.kubectl get pod -n mayastor'
ansible pc1 -m shell -a 'sudo microk8s.kubectl get mayastorpools.openebs.io -n mayastor'
ansible pc1 -m shell -a 'sudo microk8s.kubectl get pods -n mayastor --selector=app=nats'
ansible pc1 -m shell -a 'sudo microk8s.kubectl get pods -n mayastor --selector=app=etcd'
ansible pc1 -m shell -a 'sudo microk8s.kubectl get pods -n mayastor --selector=app=core-agents'
ansible pc1 -m shell -a 'sudo microk8s.kubectl get pods -n mayastor --selector=app=rest'
ansible pc1 -m shell -a 'sudo microk8s.kubectl get pods -n mayastor --selector=app=csi-controller'
ansible pc1 -m shell -a 'sudo microk8s.kubectl get pods -n mayastor --selector=app=msp-operator'
ansible pc1 -m shell -a 'sudo microk8s.kubectl get daemonset -n mayastor mayastor'
#kubectl mayastor get nodes

# Lokale Anpassungen.
kubectl patch storageclass microk8s-hostpath -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"false"}}}'
#kubectl apply -f ${indir}/openebs-ndm-config.yaml
#kubectl delete -f ${indir}/openebs-hostpath.yaml
#kubectl apply -f ${indir}/openebs-hostpath.yaml
#kubectl delete -f ${indir}/openebs-jiva-csi-default.yaml
#kubectl apply -f ${indir}/openebs-jiva-csi-default.yaml
#
${indir}/check_running_pods.sh
#
# Jetzt haben wir ClusterStorage
#

