#!/bin/bash
indir=$(dirname "$0")
# Zeigt den Maschinenstatus
ansible-playbook -v ${indir}/check_hosts.yaml
# Zeigt den k8s-Status
ansible pc -m shell -a 'microk8s status'
${indir}/check_running_pods.sh
