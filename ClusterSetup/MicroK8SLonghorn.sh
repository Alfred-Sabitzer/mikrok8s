#!/bin/bash
############################################################################################
#
# MicroK8S Konfiguration Longhorn
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
indir=$(dirname "$0")
# Longhorn Disablen
ansible pc1 -m shell -a 'microk8s.helm3 repo add longhorn https://charts.longhorn.io'
ansible pc1 -m shell -a 'microk8s.helm3 repo update '
ansible pc1 -m shell -a 'microk8s.helm3 uninstall longhorn --namespace longhorn-system '
kubectl delete namespace longhorn-system
# fstab initialisieren
ansible pc -m shell -a 'sudo curl -L https://gitlab.com/Alfred-Sabitzer/mikrok8s/-/raw/main/ClusterSetup/fstab -o /etc/fstab'
ansible pc -m shell -a 'cat /etc/fstab'
# Jetzt kommen die SSD's
ansible pc -m shell -a 'sudo umount -f /dev/sda'
ansible pc -m shell -a 'sudo wipefs -a /dev/sda'
ansible pc -m shell -a 'sudo mkfs.ext4 /dev/sda'
ansible pc -m shell -a 'sudo rm -f -R /var/data/longhorn'
ansible pc -m shell -a 'sudo mkdir -p /var/data/longhorn'
ansible pc -m shell -a 'sudo mount /dev/sda /var/data/longhorn'
ansible pc -m shell -a 'df -h | grep -i sda'
# Hinzufügen der SSD-Disks
ansible pc -m shell -a 'sudo printf $(sudo blkid -o export /dev/sda | grep UUID)" /var/data/longhorn       ext4    defaults        0       2" | sudo tee -a /etc/fstab'
ansible pc -m shell -a 'cat /etc/fstab'
# Longhorn Enablen
kubectl create namespace longhorn-system
ansible pc1 -m shell -a 'microk8s.helm3 install longhorn longhorn/longhorn --namespace longhorn-system --set csi.kubeletRootDir="/var/snap/microk8s/common/var/lib/kubelet"'
#
${indir}/check_running_pods.sh
# Lokale Anpassungen.
kubectl patch storageclass microk8s-hostpath -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"false"}}}'
kubectl patch storageclass longhorn -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'
#
ansible pc1 -m shell -a 'microk8s kubectl -n longhorn-system get pod'
# Secret
kubectl apply -f ${indir}/longhorn-system_longhorn-frontend.yaml
# Ingress
kubectl apply -f ${indir}/longhorn-ingress.yaml
#
${indir}/check_running_pods.sh
# Prüfen des Zertifikates
kubectl get certificate --all-namespaces
kubectl describe certificate --all-namespaces
kubectl get certificaterequests.cert-manager.io --all-namespaces
kubectl describe certificaterequests --all-namespaces
kubectl get certificatesigningrequests.certificates.k8s.io --all-namespaces
kubectl get Issuer --all-namespaces
kubectl get ClusterIssuer
kubectl describe ClusterIssuer letsencrypt
kubectl get challenges.acme.cert-manager.io --all-namespaces
kubectl describe challenges.acme.cert-manager.io --all-namespaces
#
# Jetzt haben wir ClusterStorage
#
