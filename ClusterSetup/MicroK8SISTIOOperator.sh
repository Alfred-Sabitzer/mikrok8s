#!/bin/bash
############################################################################################
#
# MicroK8S ISTIO Service Mesh händisch installiert
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
indir=$(dirname "$0")
ansible pc -m shell -a 'microk8s status --wait-ready'
${indir}/check_running_pods.sh
# ISTIO Disablen
#ansible pc -m shell -a 'rm -f /usr/local/bin/istioctl ' -b
#kubectl delete istiooperators.install.istio.io -n $( kubectl get --all-namespaces istiooperators.install.istio.io | grep -v NAMESPACE | awk '{print $1 " " $2} ')
#kubectl delete namespace istio-system
#kubectl delete namespace istio-operator
kubectl label namespace default istio-injection-
kubectl label namespace admin istio-injection-
kubectl label namespace slainte istio-injection-
${indir}/cluster_reboot.sh
ansible pc1 -m shell -a 'istioctl operator remove --force'
ansible pc1 -m shell -a 'istioctl manifest generate | kubectl delete -f - '
# das bleibt hängen
kubectl delete istiooperators.install.istio.io -n $( kubectl get --all-namespaces istiooperators.install.istio.io | grep -v NAMESPACE | awk '{print $1 " " $2} ') --force &
NS=`kubectl get ns |grep Terminating | awk 'NR==1 {print $1}'` && kubectl get namespace "$NS" -o json   | tr -d "\n" | sed "s/\"finalizers\": \[[^]]\+\]/\"finalizers\": []/"   | kubectl replace --raw /api/v1/namespaces/$NS/finalize -f -
kubectl delete ns istio-operator --grace-period=0 --force &
NS=`kubectl get ns |grep Terminating | awk 'NR==1 {print $1}'` && kubectl get namespace "$NS" -o json   | tr -d "\n" | sed "s/\"finalizers\": \[[^]]\+\]/\"finalizers\": []/"   | kubectl replace --raw /api/v1/namespaces/$NS/finalize -f -
kubectl delete ns istio-system --grace-period=0 --force &
NS=`kubectl get ns |grep Terminating | awk 'NR==1 {print $1}'` && kubectl get namespace "$NS" -o json   | tr -d "\n" | sed "s/\"finalizers\": \[[^]]\+\]/\"finalizers\": []/"   | kubectl replace --raw /api/v1/namespaces/$NS/finalize -f -
sleep 1m
${indir}/cluster_reboot.sh
# Sicher ist sicher
kubectl delete ns istio-operator --grace-period=0 --force &
NS=`kubectl get ns |grep Terminating | awk 'NR==1 {print $1}'` && kubectl get namespace "$NS" -o json   | tr -d "\n" | sed "s/\"finalizers\": \[[^]]\+\]/\"finalizers\": []/"   | kubectl replace --raw /api/v1/namespaces/$NS/finalize -f -
kubectl create namespace istio-operator
# Sicher ist sicher
kubectl delete ns istio-system --grace-period=0 --force &
NS=`kubectl get ns |grep Terminating | awk 'NR==1 {print $1}'` && kubectl get namespace "$NS" -o json   | tr -d "\n" | sed "s/\"finalizers\": \[[^]]\+\]/\"finalizers\": []/"   | kubectl replace --raw /api/v1/namespaces/$NS/finalize -f -
kubectl create namespace istio-system
# Secrets erneuern
kubectl apply -f ../SecureRegistry/dockerbasicjson.yaml
kubectl apply -f ../Registry/dockerconfigjson.yaml
# ISTIO Enablen
ansible pc -m shell -a 'microk8s config > /home/alfred/.kube/config '
command="src=${indir}/Istio_Install.sh dest=/home/alfred/Istio_Install.sh mode=0755 force=yes"
ansible pc1 -m copy -a "${command}"
ansible pc1 -m shell -a './Istio_Install.sh '
${indir}/check_running_pods.sh
kubectl apply -f ${indir}/ISTIOOperator.yaml
#
# Jetzt haben wir einen Servicemesh
#