#!/bin/bash
indir=$(dirname "$0")
# Zeigt den Maschinenstatus
ansible-playbook -v ${indir}/check_hosts.yaml
# synchronisiert die Zeiten
ansible pc -m shell -a 'sudo systemctl stop systemd-timesyncd.service'
ansible pc -m shell -a 'sudo systemctl start systemd-timesyncd.service'
ansible pc -m shell -a 'sudo systemctl status systemd-timesyncd.service'
sleep 10
# Zeigt die Uhrzeit
ansible pc -m shell -a 'date'
