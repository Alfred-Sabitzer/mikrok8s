#!/bin/bash
############################################################################################
#
# MicroK8S holen der Login-Daten
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
# Verbinden der Nodes
rm -rf ${HOME}/.kube
mkdir -p ${HOME}/.kube
ansible pc1 -m shell -a "microk8s config" | tail -n +2 > ${HOME}/.kube/config
# jeder User (alfred und ansible) muß das für sich machen, damit das lokale kubectl geht
#ansible pc -m shell -a "microk8s config > /home/alfred/.kube/config "
#ansible pc -m shell -a "microk8s config > /home/ansible/.kube/config "
#
# Jetzt funktioniert das lokal installierte kubectl, sowie lens und andere
#
