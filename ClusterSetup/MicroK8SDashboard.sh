#!/bin/bash
############################################################################################
#
# MicroK8S Einrichten des Dashboards, Prometheus und Metrics Services
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
indir=$(dirname "$0")
# Disablen
ansible pc1 -m shell -a 'microk8s disable dashboard-ingress  '  # Ingress definition for Kubernetes dashboard
ansible pc1 -m shell -a 'microk8s disable dashboard  '          # The Kubernetes dashboard
ansible pc1 -m shell -a 'microk8s disable prometheus '          # Prometheus operator for monitoring and logging
ansible pc1 -m shell -a 'microk8s disable metrics-server '      # K8s Metrics Server for API access to service metrics
kubectl delete -f ${indir}/dashboard-service-account.yaml       # Token für den Serviceaccount
kubectl delete -f ${indir}/grafana-ingress.yaml                 # Ingress-Definition
kubectl delete -f ${indir}/prometheus-ingress.yaml              # Ingress-Definition
kubectl delete -f ${indir}/monitoring_grafana-frontend.yaml     # Secret
kubectl delete -f ${indir}/monitoring_grafana-frontend-api.yaml # Secret
ansible pc -m shell -a 'microk8s status --wait-ready'
${indir}/check_running_pods.sh
${indir}/cluster_reboot.sh
ansible-playbook -v ${indir}/check_hosts.yaml
ansible pc -m shell -a 'sudo /root/MicroK8S_Start.sh '
${indir}/check_running_pods.sh
# Enable
ansible pc1 -m shell -a 'microk8s enable metrics-server '      # K8s Metrics Server for API access to service metrics
${indir}/cluster_reboot.sh
ansible-playbook -v ${indir}/check_hosts.yaml
ansible pc -m shell -a 'sudo /root/MicroK8S_Start.sh '
${indir}/check_running_pods.sh
ansible pc1 -m shell -a 'microk8s enable prometheus '          # Prometheus operator for monitoring and logging
ansible pc1 -m shell -a 'microk8s enable dashboard  '          # The Kubernetes dashboard
kubectl apply -f ${indir}/dashboard-service-account.yaml       # Token für den Serviceaccount
kubectl apply -f ${indir}/monitoring_grafana-frontend.yaml     # Secret
kubectl apply -f ${indir}/monitoring_grafana-frontend-api.yaml # Secret
kubectl apply -f ${indir}/grafana-ingress.yaml                 # Ingress-Definition
kubectl apply -f ${indir}/prometheus-ingress.yaml              # Ingress-Definition
ansible pc -m shell -a 'microk8s enable community '            # Ermöglichen der Zusatzfeatures
ansible pc1 -m shell -a 'microk8s enable dashboard-ingress  '  # Ingress definition for Kubernetes dashboard
kubectl apply -f ${indir}/namespaces.yaml                      # Einspielen der Standard-Namespaces
kubectl apply -f ${indir}/clusterrole-prometheus-k8s.yaml      # Erweiterte Rechte für den Prometheus
ansible pc -m shell -a 'microk8s status --wait-ready'
# Anzeigen der Tokens
#kubectl -n kube-system describe secret $(microk8s kubectl -n kube-system get secret | grep default-token | cut -d " " -f1)
#kubectl -n kube-system get secret $(kubectl -n kube-system get sa/admin-user -o jsonpath="{.secrets[0].name}") -o go-template="{{.data.token | base64decode}}"
#kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep admin-user | awk '{print \$1}')
${indir}/check_running_pods.sh
#
# Jetzt sind alle Standard-Services verfügbar
#
# kubernetes-dashboard.127.0.0.1.nip.io in die /etc/hosts Datei eintragen, und man kann über den Ingress auf das Dashboard zugreifen
#