#!/bin/bash
############################################################################################
#
# MicroK8S Einrichten des Loadbalancers
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
indir=$(dirname "$0")
# metallb konfigurieren
ansible pc1 -m shell -a 'microk8s enable metallb:192.168.0.210-192.168.0.244'
ansible pc -m shell -a 'microk8s status --wait-ready'
#
${indir}/check_running_pods.sh
#