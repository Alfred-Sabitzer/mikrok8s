#!/bin/bash
############################################################################################
#
# MicroK8S OpenEBS https://microk8s.io/docs/addon-openebs
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
indir=$(dirname "$0")
${indir}/check_running_pods.sh
# Disable OpenEBS
ansible pc1 -m shell -a 'microk8s disable openebs:force'
# fstab initialisieren
ansible pc -m shell -a 'sudo curl -L https://gitlab.com/Alfred-Sabitzer/mikrok8s/-/raw/main/ClusterSetup/fstab -o /etc/fstab'
ansible pc -m shell -a 'cat /etc/fstab'
# Installieren der Sourcen
ansible pc -m shell -a 'sudo apt-get -y install open-iscsi'
ansible pc -m shell -a 'sudo systemctl enable iscsid'
ansible pc -m shell -a 'sudo systemctl start iscsid'
ansible pc -m shell -a 'sudo systemctl status iscsid'
# Jetzt kommen die SSD's
ansible pc -m shell -a 'sudo umount -f /dev/sda'
ansible pc -m shell -a 'sudo wipefs -a /dev/sda'
ansible pc -m shell -a 'sudo mkfs.ext4 /dev/sda'
ansible pc -m shell -a 'sudo rm -f -R /var/data/openebs'
ansible pc -m shell -a 'sudo mkdir -p /var/data/openebs'
ansible pc -m shell -a 'sudo mount /dev/sda /var/data/openebs'
ansible pc -m shell -a 'df -h | grep -i sda'
# Hinzufügen der SSD-Disks
ansible pc -m shell -a 'sudo printf $(sudo blkid -o export /dev/sda | grep UUID)" /var/data/openebs       ext4    defaults        0       2" | sudo tee -a /etc/fstab'
ansible pc -m shell -a 'cat /etc/fstab'
# Enable OpenEBS
ansible pc1 -m shell -a 'microk8s enable openebs'
# Patchen der Default-Class
kubectl patch storageclass microk8s-hostpath -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"false"}}}'
kubectl patch storageclass openebs-jiva-csi-default -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'
# Setzen des Basis-Pfades auf die SSD
kubectl get storageclass openebs-hostpath -o yaml > /tmp/openebs-hostpath.yaml
sed --in-place  "s,/var/snap/microk8s/common/var/openebs/local,/var/data/openebs,g" /tmp/openebs-hostpath.yaml
kubectl apply -f /tmp/openebs-hostpath.yaml
rm -f /tmp/openebs-hostpath.yaml
#
${indir}/check_running_pods.sh
#
# Jetzt haben wir ClusterStorage
#
