#!/bin/bash
############################################################################################
#
# Lokale Host-Id in das hosts-File eintragen
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#
curl -L https://gitlab.com/Alfred-Sabitzer/mikrok8s/-/raw/main/ClusterSetup/hosts -o /etc/hosts
Node=$(cat /etc/hostname)
NODEIP=$(cat /etc/hosts | grep -i ${Node})
sf="   ${Node}"
NODEIP=${NODEIP%"$sf"*}
echo ${NODEIP} > ./nodeip.txt
sed -i "s,#NODE#,${Node},g" /etc/hosts
#