#!/bin/bash
############################################################################################
#
# Schnell-Installation microk8s
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
indir=$(dirname "$0")
#
# Vorraussetzung: Sauber installierte Nodes
# Ansible Account verfügbar
# Lokal vorhandenes Kubectl, kpt, kustomize, docker
#
${indir}/MicroK8SInit.sh
${indir}/MicroK8SCluster.sh
${indir}/MicroK8SKube.sh
${indir}/MicroK8Smetallb.sh
${indir}/MicroK8SHelm.sh
${indir}/MicroK8SIngress.sh
${indir}/MicroK8SCertManager.sh
#${indir}/MicroK8SOpenEBS.sh
${indir}/MicroK8SLonghorn.sh
#${indir}/MicroK8SRegistry.sh - Da läuft was schief
${indir}/MicroK8SDashboard.sh
${indir}/MicroK8SGrafanaReports.sh
#${indir}/MicroK8SK8TZ.sh - reagiert nicht auf injectAll=false
${indir}/MicroK8SSecureRegistry.sh # Passworte
${indir}/MicroK8S_Registry.sh # Wir arbeiten mit der Secure Registry
#${indir}/MicroK8SISTIO.sh - wird auf Raspberry nicht supported
#${indir}/MicroK8SISTIOOperator.sh - wird auf Raspberry nicht supported
${indir}/MicroK8SLinkerd.sh
#
# Nun ist der Cluster bereit
#
exit
#
