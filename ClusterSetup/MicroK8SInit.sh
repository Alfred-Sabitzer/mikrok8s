#!/bin/bash
############################################################################################
#
# Schnell-Installation microk8s
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#
indir=$(dirname "$0")
ansible-playbook -v ${indir}/check_hosts.yaml
# Environmentvariable - Wird für Python3 gebraucht
ansible pc -m  shell -a "sudo rm -f /root/MicroK8S_set_environment.sh"
ansible pc -m  shell -a "sudo curl -L https://gitlab.com/Alfred-Sabitzer/mikrok8s/-/raw/main/ClusterSetup/MicroK8S_set_environment.sh -o /root/MicroK8S_set_environment.sh"
ansible pc -m  shell -a "sudo chmod 755 /root/MicroK8S_set_environment.sh"
ansible pc -m  shell -a "sudo /root/MicroK8S_set_environment.sh"
# Modules für den Maystor
ansible pc -m  shell -a "sudo apt install linux-modules-extra-\$(uname -r)"
# Systemupdate
ansible pc -m shell -a 'sudo timedatectl set-timezone Europe/Vienna '
ansible pc -m shell -a 'date '
ansible pc -m shell -a 'sudo apt-get update'
ansible pc -m shell -a 'sudo apt-get upgrade -y'
ansible pc -m shell -a 'sudo microk8s stop '
ansible pc -m shell -a 'sudo microk8s status --wait-ready'
ansible pc -m shell -a 'sudo shutdown -r now'
sleep 2m
ansible-playbook -v ${indir}/check_hosts.yaml
# Installation der notwendigen Software
ansible pc -m shell -a 'sudo apt-get -y install open-iscsi'
ansible pc -m shell -a 'sudo systemctl enable iscsid'
ansible pc -m shell -a 'sudo systemctl start iscsid'
ansible pc -m shell -a 'sudo systemctl status iscsid'
ansible pc -m shell -a 'sudo apt-get install -y mc sshfs tree'
ansible pc -m shell -a 'sudo apt-get install bash-completion -y'
# https://longhorn.io/docs/1.1.1/deploy/install/
ansible pc -m shell -a 'sudo apt-get install curl util-linux jq nfs-common linux-modules-extra-raspi -y '
# Jetzt kommt microk8s
ansible pc -m shell -a 'sudo microk8s stop '
ansible pc -m shell -a 'sudo microk8s status --wait-ready'
ansible pc -m shell -a 'sudo snap remove microk8s '
ansible pc -m shell -a 'sudo rm -rf /var/snap/microk8s'
ansible pc -m shell -a 'sudo umount /dev/sda'
ansible pc -m shell -a 'sudo rm -rf /var/data/longhorn '
ansible pc -m shell -a 'sudo rm -rf /var/data/openebs '
ansible pc -m shell -a 'sudo rm -rf /var/lib/longhorn '
ansible pc -m shell -a 'sudo rm -rf /home/alfred/.kube '
ansible pc -m shell -a 'sudo rm -rf /home/ansible/.kube '
# fstab initialisieren
ansible pc -m  shell -a "sudo rm -f /etc/fstab"
ansible pc -m shell -a 'sudo curl -L https://gitlab.com/Alfred-Sabitzer/mikrok8s/-/raw/main/ClusterSetup/fstab -o /etc/fstab'
# hosts initialisieren
ansible pc -m  shell -a "sudo rm -f /root/replace_host_id.sh"
ansible pc -m  shell -a "sudo curl -L https://gitlab.com/Alfred-Sabitzer/mikrok8s/-/raw/main/ClusterSetup/replace_host_id.sh -o /root/replace_host_id.sh"
ansible pc -m  shell -a "sudo chmod 755 /root/replace_host_id.sh"
ansible pc -m shell -a 'sudo /root/replace_host_id.sh '
#
ansible pc -m shell -a 'sudo shutdown -r now'
sleep 2m
ansible-playbook -v ${indir}/check_hosts.yaml
sleep 1m
# Installieren microk8s
ansible pc -m  shell -a "sudo rm -f /root/MicroK8S_Install.sh"
ansible pc -m  shell -a "sudo curl -L https://gitlab.com/Alfred-Sabitzer/mikrok8s/-/raw/main/ClusterSetup/MicroK8S_Install.sh -o /root/MicroK8S_Install.sh"
ansible pc -m  shell -a "sudo chmod 755 /root/MicroK8S_Install.sh"
ansible pc -m  shell -a "sudo rm -f /root/MicroK8S_Start.sh"
ansible pc -m  shell -a "sudo curl -L https://gitlab.com/Alfred-Sabitzer/mikrok8s/-/raw/main/ClusterSetup/MicroK8S_Start.sh -o /root/MicroK8S_Start.sh"
ansible pc -m  shell -a "sudo chmod 755 /root/MicroK8S_Start.sh"
#
ansible pc -m shell -a 'sudo /root/MicroK8S_Install.sh '
#
ansible pc -m shell -a 'sudo shutdown -r now'
#
sleep 2m
#
ansible-playbook -v ${indir}/check_hosts.yaml
# Starten microk8s
ansible pc -m shell -a 'sudo /root/MicroK8S_Start.sh '

ansible pc -m shell -a 'microk8s enable dns rbac ha-cluster'
ansible pc -m shell -a 'microk8s status --wait-ready'
ansible pc -m shell -a 'microk8s enable community '            # Ermöglichen der Zusatzfeatures
ansible pc -m shell -a 'microk8s status --wait-ready'
#
# Nun müssen die Nodes zu einem cluster verbunden werden
# microk8s add-node
#


