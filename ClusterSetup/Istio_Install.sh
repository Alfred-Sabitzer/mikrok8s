#!/bin/bash
############################################################################################
#
# MicroK8S ISTIO Service Mesh installieren
# https://github.com/querycap/istio
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
indir=$(dirname "$0")
cd ${HOME}
installdir=$(ls | grep -i istio-)
if [[ ! "${installdir} " == " " ]]
then
  rm -Rf ${installdir}
fi
curl -L https://istio.io/downloadIstio | sh -
installdir=$(ls | grep -i istio-)
cd ${installdir}
ls -lisa
microk8s.kubectl create namespace istio-operator
microk8s.kubectl create namespace istio-system
sudo cp ./bin/istioctl /usr/local/bin
istioctl operator init --hub=monitoring.slainte.at:5000/querycapistio --imagePullSecrets dockerbasicjson
istioctl manifest generate --set profile=demo > istio.yaml
sed --in-place  "s,amd64,arm64,g" istio.yaml
sed --in-place  "s,policy/v1beta1,policy/v1,g" istio.yaml
sed --in-place  "s,docker.io/istio,monitoring.slainte.at:5000/querycapistio,g" istio.yaml
sed --in-place  's,imagePullSecrets": \[\],imagePullSecrets": "dockerbasicjson",g' istio.yaml
# Jetzt müssen wir noch die Pull-Secrets in das yaml einfügen
cp istio.yaml istio.input
echo "#" > istio.yaml
echo "#  Einfügen der richtigen Pull-Secrets " >> istio.yaml
echo "#       imagePullSecrets: " >> istio.yaml
echo "#          - name: dockerbasicjson" >> istio.yaml
echo "#" >> istio.yaml
# Iterate the string array using for loop
while IFS= read -r line
do
  if [[ "$line" == *"containers:"* ]]; then
    echo "#${line}#"
    tmp=${line%*containers:}   # remove suffis starting with
    #echo "1${tmp}1"
    echo "${tmp}imagePullSecrets:" >> istio.yaml
    echo "${tmp}   - name: dockerbasicjson" >> istio.yaml
  fi
#  "imagePullSecrets": [],
#  dockerbasicjson
  echo "$line" >> istio.yaml
done < istio.input
#
microk8s.kubectl create -f ./istio.yaml
# Sicher ist sicher
microk8s.kubectl apply -f ./istio.yaml
#
# Nun restarten wir alle Deployments im Istio-Operator und Istio-System
#
kubectl label namespace default istio-injection=enabled
kubectl label namespace admin istio-injection=enabled
kubectl label namespace slainte istio-injection=enabled
#
# Nun restarten wir alle Deployments im unseren Mesh-Namespaces
#

#
# Nun läuft der Servicemesh
#