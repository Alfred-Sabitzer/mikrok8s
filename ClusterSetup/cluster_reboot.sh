#!/bin/bash
############################################################################################
#
# Reboot des Clusters
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
indir=$(dirname "$0")
# Rebooted alles nodes im Cluster
ansible pc -m shell -a 'microk8s stop'
ansible pc -m shell -a 'microk8s status --wait-ready'
ansible pc -m shell -a 'sudo shutdown -r now'
sleep 1m
# Zeigt den Maschinenstatus
ansible-playbook -v ${indir}/check_hosts.yaml
ansible pc -m shell -a 'sudo /root/MicroK8S_Start.sh '
${indir}/check_running_pods.sh
