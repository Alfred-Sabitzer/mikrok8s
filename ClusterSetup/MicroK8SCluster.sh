#!/bin/bash
############################################################################################
#
# MicroK8S - Verbinden der Nodes zu einem Cluster
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
# Verbinden der Nodes
JOIN=$(ansible pc1 -m shell -a 'microk8s add-node' | tail -n 1)
ansible pc2 -m shell -a "${JOIN}"
JOIN=$(ansible pc1 -m shell -a 'microk8s add-node' | tail -n 1)
ansible pc3 -m shell -a "${JOIN}"
JOIN=$(ansible pc1 -m shell -a 'microk8s add-node' | tail -n 1)
ansible pc4 -m shell -a "${JOIN}"
JOIN=$(ansible pc1 -m shell -a 'microk8s add-node' | tail -n 1)
ansible pc5 -m shell -a "${JOIN}"
#
ansible pc -m shell -a "microk8s status"
#
# Jetzt haben wir einen Cluster
#
