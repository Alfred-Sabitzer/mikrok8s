#!/bin/bash
############################################################################################
#
# MicroK8S Konfiguration Helm
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
indir=$(dirname "$0")
# Erst disablen
ansible pc -m shell -a 'microk8s disable helm3'
# Helm enablen
ansible pc -m shell -a 'microk8s status --wait-ready'
ansible pc -m shell -a 'microk8s enable helm3'
#
# Logischer Link (kann für alle gut sein).
#
ansible pc -m shell -a 'sudo sudo snap unalias helm '
ansible pc -m shell -a 'sudo sudo snap alias microk8s.helm3 helm '
#
${indir}/check_running_pods.sh
#
# Jetzt können wir Helm benutzen
#

