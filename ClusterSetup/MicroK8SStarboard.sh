#!/bin/bash
############################################################################################
#
# Schnell-Installation microk8s Starboard # (community) Kubernetes-native security toolkit
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
#
indir=$(dirname "$0")
ansible-playbook -v ${indir}/check_hosts.yaml
# Jetzt kommt microk8s
# Disablen
ansible pc1 -m shell -a 'microk8s disable starboard'           # Disablen (community) Kubernetes-native security toolkit
${indir}/check_running_pods.sh
# Enable
ansible pc1 -m shell -a 'microk8s enable starboard'             # Enablen (community) Kubernetes-native security toolkit
${indir}/check_running_pods.sh
