#!/bin/bash
############################################################################################
#
# Starten des Clusters
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
indir=$(dirname "$0")
# Zeigt den Maschinenstatus
ansible-playbook -v ${indir}/check_hosts.yaml
# Startet alle Nodes im Cluster
ansible pc -m shell -a 'sudo /root/MicroK8S_Start.sh '
${indir}/check_running_pods.sh