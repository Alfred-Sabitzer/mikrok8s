#!/bin/bash
############################################################################################
#
# Konfiguration für die Secure BasisRegistry
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
indir=$(dirname "$0")
workdir=$(pwd)
cd ../SecureRegistry
./create_dockerbasicjson.sh
cd ${workdir}
${indir}/check_running_pods.sh
#
# Jetzt haben wir eine gesicherte Registry
#

