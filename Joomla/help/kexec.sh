#!/bin/sh
# Connect in den Joomla Container
mypod=$(kubectl get pod -n slainte | grep -i joomla-nginx | awk '{print $1 }')
kubectl exec -i -t -n slainte ${mypod} -c joomla-nginx "--" sh -c "clear; (bash || ash || sh)"
