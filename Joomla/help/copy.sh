#!/bin/sh
# Kopieren von Daten aus dem joomla-Container
mypod=$(kubectl get pod -n slainte | grep -i joomla| awk '{print $1 }')
kubectl cp -n slainte ${mypod}:/usr/local/etc/ ./n/ -c joomla-app
