# Joomla Installation

Joomla mit Redis und Postgres. 

# Installation

```bash
./docker/do.sh
```
Damit wird das kokrete Image erzeugt, und im K8S-Docker-Repository abgelegt.

Diese Installation benutzt das fpm-Image. Dh. es braucht einen zweiten Container für den nginx.

# Vorbereitung

Die Postgres-Role und die Postgres-Datenbank müssen bereits existieren.
