#!/bin/sh
############################################################################################
#
# Prüfen, ob nginx bereits läuft
#
############################################################################################"
#
app=$(ps -ef | grep -i 'nginx: master process nginx' | grep -v grep)
if [ "${app}x"  == "x"  ] ; then # php-fpm läuft noch nicht
  exit 1
fi
#