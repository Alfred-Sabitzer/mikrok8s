#!/bin/sh
############################################################################################
# Inspiration: https://github.com/redis/redis/blob/unstable/utils/gen-test-certs.sh
# Generiere Zertifikate
#
#   ${CERTDIR}/ca.{crt,key}          Self signed CA certificate.
#   ${CERTDIR}/redis.{crt,key}       A certificate with no key usage/policy restrictions.
#   ${CERTDIR}/client.{crt,key}      A certificate restricted for SSL client usage.
#   ${CERTDIR}/server.{crt,key}      A certificate restricted for SSL server usage.
#   ${CERTDIR}/redis.dh              DH Params file.
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
#shopt -o -s nounset #-No Variables without definition

NODAYS=3650
FULLHOST="www.nextcloud.k8s.slainte.at"
KEYLEN=4096
CERTDIR="./certs"
#CERTDIR="tls/certs"  # Das ist für lokale Tests

generate_cert() {
    name=$1
    cn="$2"
    opts="$3"

    keyfile=${CERTDIR}/${name}.key
    certfile=${CERTDIR}/${name}.crt

    [ -f $keyfile ] || openssl genrsa -out $keyfile ${KEYLEN}
    openssl req \
        -new -sha256 \
        -subj "/C=CA/ST=QC/O=Slainte/CN=$cn" \
        -key $keyfile | \
        openssl x509 \
            -req -sha256 \
            -CA ${CERTDIR}/ca.crt \
            -CAkey ${CERTDIR}/ca.key \
            -CAserial ${CERTDIR}/ca.txt \
            -CAcreateserial \
            -days ${NODAYS} \
            $opts \
            -out $certfile
}

mkdir -p ${CERTDIR}
[ -f ${CERTDIR}/ca.key ] || openssl genrsa -out ${CERTDIR}/ca.key ${KEYLEN}
openssl req \
    -x509 -new -nodes -sha256 \
    -key ${CERTDIR}/ca.key \
    -days ${NODAYS} \
    -subj '/C=CA/ST=QC/O=Slainte/CN=Certificate Authority' \
    -out ${CERTDIR}/ca.crt

cat > ${CERTDIR}/openssl.cnf <<_END_
[ server_cert ]
keyUsage = digitalSignature, keyEncipherment
nsCertType = server
[ client_cert ]
keyUsage = digitalSignature, keyEncipherment
nsCertType = client
_END_

generate_cert server "${FULLHOST} - Server-only" "-extfile ${CERTDIR}/openssl.cnf -extensions server_cert"
generate_cert client "${FULLHOST} - Client-only" "-extfile ${CERTDIR}/openssl.cnf -extensions client_cert"
generate_cert nextcloud "${FULLHOST} - Generic-cert" " "

[ -f ${CERTDIR}/redis.dh ] || openssl dhparam -out ${CERTDIR}/dhparam.pem 1024

echo "Alle Zertifikate generiert"
ls -lsia ${CERTDIR}