#!/bin/sh
############################################################################################
#
# Konfiguration und Update von Joomla
# https://docs.joomla.org/J4.x:Joomla_CLI_Installation
# https://magazine.joomla.org/all-issues/june-2022/joomla-4-a-powerful-cli-application
#
############################################################################################"
#
cd /var/www/html
pwd

exit 0
# vorerst keine Autmoatische Konfiguration

# Warten bis die Applikation läuft
while [ "${app}x"  == "x"  ]
do
	echo "`date` [`hostname`] warte auf den Master-Prozess"
	sleep $((60 + RANDOM % 11));
  app=$(ps -ef | grep -i 'php-fpm: master process' | grep -v grep)
done

FILE="/var/www/html/configuration.php"
if [ ! -f "$FILE" ]; then
    echo "$FILE existiert nicht."
    exit 1
fi


#Current options in Configuration
#================================
#
# ------------------------------- ----------------------------------------------------------------------------------
#  Option                          Value
# ------------------------------- ----------------------------------------------------------------------------------
#  offline                         false
#  offline_message                 Diese Website ist zurzeit im Wartungsmodus.<br />Bitte später wiederkommen.
#  display_offline_message         1
#  offline_image
#  sitename                        Verein zur Förderung des Wohlbefindens
#  editor                          tinymce
#  captcha                         0
#  list_limit                      20
#  access                          1
#  debug                           false
#  debug_lang                      false
#  debug_lang_const                true
#  dbtype                          pgsql
#  host                            postgresdb-svc.slainte.svc.cluster.local
#  user                            joomlaadmin
#  password                        eOvq5wBEgchqjQUtC67Wn7zfrV3gNh3pXtjbR0eC
#  db                              joomlaadmin
#  dbprefix                        j4_
#  dbencryption                    0
#  dbsslverifyservercert           false
#  dbsslkey
#  dbsslcert
#  dbsslca
#  dbsslcipher
#  force_ssl                       0
#  live_site
#  secret                          5bgB8d8tg90Tn71S
#  gzip                            true
#  error_reporting                 default
#  helpurl                         https://help.joomla.org/proxy?keyref=Help{major}{minor}:{keyref}&lang={langcode}
#  offset                          Europe/Vienna
#  mailonline                      true
#  mailer                          smtp
#  mailfrom                        microk8s@slainte.at
#  fromname                        Verein zur Förderung des Wohlbefindens
#  sendmail                        /usr/sbin/sendmail
#  smtpauth                        true
#  smtpuser                        microk8s@slainte.at
#  smtppass                        3orLGdGB1AHhdEQWaUrNpWgLOsRxhQ
#  smtphost                        smtp.slainte.at
#  smtpsecure                      tls
#  smtpport                        587
#  caching                         2
#  cache_handler                   redis
#  cachetime                       15
#  cache_platformprefix            true
#  MetaDesc
#  MetaAuthor                      true
#  MetaVersion                     false
#  robots
#  sef                             true
#  sef_rewrite                     false
#  sef_suffix                      false
#  unicodeslugs                    false
#  feed_limit                      10
#  feed_email                      none
#  log_path                        /var/www/html/administrator/logs
#  tmp_path                        /var/www/html/tmp
#  lifetime                        15
#  session_handler                 redis
#  shared_session                  false
#  session_metadata                true
#  memcached_persist               true
#  memcached_compress              false
#  memcached_server_host           localhost
#  memcached_server_port           11211
#  redis_persist                   true
#  redis_server_host               redis.slainte.svc.cluster.local
#  redis_server_port               6379
#  redis_server_db                 1
#  cors                            false
#  cors_allow_origin               *
#  cors_allow_headers              Content-Type,X-Joomla-Token
#  cors_allow_methods
#  behind_loadbalancer             false
#  proxy_enable                    false
#  proxy_host
#  proxy_port
#  proxy_user
#  massmailoff                     false
#  replyto
#  replytoname
#  MetaRights
#  sitename_pagetitles             0
#  session_filesystem_path
#  session_memcached_server_host   localhost
#  session_memcached_server_port   11211
#  session_redis_persist           1
#  session_redis_server_host       redis.slainte.svc.cluster.local
#  session_redis_server_port       6379
#  session_redis_server_db         2
#  session_metadata_for_guest      true
#  frontediting                    1
#  log_everything                  1
#  log_deprecated                  0
#  log_priorities                  ["emergency","alert","critical","error"]
#  log_categories
#  log_category_mode               0
#  cookie_domain
#  cookie_path
#  asset_id                        1
#  redis_server_auth               2rqx7zvaoko5sShRVsjPQ3M52pDt2u
#  session_redis_server_auth       2rqx7zvaoko5sShRVsjPQ3M52pDt2u
#  csp_nonce                       Not Set
# ------------------------------- ----------------------------------------------------------------------------------

echo "Update der Konfiguration"
php ./cli/joomla.php config:set host=${JOOMLA_DB_HOST}
php ./cli/joomla.php config:set user=${JOOMLA_DB_USER}
php ./cli/joomla.php config:set password=${JOOMLA_DB_PASSWORD}
php ./cli/joomla.php config:set db=${JOOMLA_DB_NAME}
php ./cli/joomla.php config:set db=${JOOMLA_DB_USER}
php ./cli/joomla.php config:set dbprefix="j4_"
php ./cli/joomla.php config:set offset="Europe/Vienna"
php ./cli/joomla.php config:set mailonline=true
php ./cli/joomla.php config:set mailer="smtp"
php ./cli/joomla.php config:set mailfrom="${JOOMLA_ADMIN_MAIL_USERNAME}"
php ./cli/joomla.php config:set fromname="Verein zur Förderung des Wohlbefindens"
php ./cli/joomla.php config:set sendmail="/usr/sbin/sendmail"
php ./cli/joomla.php config:set smtpauth="true"
php ./cli/joomla.php config:set smtpuser="${JOOMLA_ADMIN_MAIL_USERNAME}"
php ./cli/joomla.php config:set smtppass="${JOOMLA_ADMIN_MAIL_PASSWORD}"
php ./cli/joomla.php config:set smtphost="smtp.slainte.at"
php ./cli/joomla.php config:set smtpsecure="tls"
php ./cli/joomla.php config:set smtpport="587"
php ./cli/joomla.php config:set caching="2"
php ./cli/joomla.php config:set cache_handler="redis"
php ./cli/joomla.php config:set session_handler="redis"
php ./cli/joomla.php config:set redis_persist="true"
php ./cli/joomla.php config:set redis_server_host="${REDIS_HOST}"
php ./cli/joomla.php config:set redis_server_port="6379"
php ./cli/joomla.php config:set redis_server_db="1"
php ./cli/joomla.php config:set redis_server_auth="${REDIS_PASSWORD}"
php ./cli/joomla.php config:set session_redis_server_auth="${REDIS_PASSWORD}"
php ./cli/joomla.php config:set session_redis_persist="1"
php ./cli/joomla.php config:set session_redis_server_host="${REDIS_HOST}"
php ./cli/joomla.php config:set session_redis_server_port="6379"
php ./cli/joomla.php config:set session_redis_server_db="2"
php ./cli/joomla.php config:set session_metadata_for_guest=true
#
php ./cli/joomla.php user:reset-password --username ${JOOMLA_ADMIN_USER} --password "${JOOMLA_ADMIN_PASSWORD}"
# https://docs.joomla.org/J4.x:CLI_Update
# https://www.ezone.co.uk/blog/joomla-cli.html
#
#php cli/joomla.php core:check-updates
php cli/joomla.php core:update
echo "Joomla ist richtig konfiguriert"
#
# Jetzt ist Joomla am neuesten Stand