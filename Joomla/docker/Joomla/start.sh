#!/bin/sh
echo "############################################################################################"
echo "#"
echo "# Starten der Joomla-Instanz mit den richtigen Werten."
echo "#"
echo "############################################################################################"
#
# fpm wird als Daemon gestartet
/entrypoint.sh php-fpm --fpm-config /usr/local/etc/php/php-fpm.conf --daemonize=yes &
/Joomla_Config.sh
#
/dummy.sh