# Starboard

Installation der Community-Security-Suite https://aquasecurity.github.io/starboard/v0.15.6/

# Installation

```bash
./ClusterSetup/MicroK8SStarboard.sh 
```

Damit wird starboard installiert und steht zur Verfügung.

Inspect created VulnerabilityReports by:

```bash
kubectl get vulnerabilityreports --all-namespaces -o wide
```

Inspect created ConfigAuditReports by:

```bash
kubectl get configauditreports --all-namespaces -o wide
```

Inspect created CISKubeBenchReports by:

```bash
kubectl get ciskubebenchreports -o wide
```

Inspect the work log of starboard-operator by:

```bash
kubectl logs -n starboard-system deployment/starboard-operator
```
