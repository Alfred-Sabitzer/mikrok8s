# OpenEBS-Demo

Demonstration wie mit der OpenEBS-Software eine PVC erzeugt werden kann.

## Vorbereitung

Es ist eine korrekte Installation des mikroK8S Features notwendig.

```bash
ClusterSetup/MicroK8SOpenEBS.sh
```

## Starten

```bash
kubectl apply -f .
```
