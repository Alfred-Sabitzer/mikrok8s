# Hello World

Das ist ein Testbeispiel für ein Hello-World.

## Vorbereitung

Es muß einen existierenden Cluster geben, und dort muß eine Docker-Registry laufen.

## Skript um zu bauen

Das bauen des Docker-Images muß auf dem Raspberry-PI-Build-Rechner erfolgen.

```
./do.sh
```

Dieses Skript baut das Docker-Image und checkt es in die richtige Registry ein.

## Einspielen des Kubernetes Services

Der Service kann sehr leicht eingespielt werden.

```
kubectl apply -f .
ingress.networking.k8s.io/hello-world created
service/hello-world-svc created
deployment.apps/hello-world-depl created

```
Die Kontrolle des Deployments erfolgt dann mit

```
kubectl get all -n slainte | grep -i hello
pod/hello-world-depl-7cb48686b-lrk6x   1/1     Running   0          2m41s
service/hello-world-svc   ClusterIP   10.152.183.196   <none>        80/TCP    2m42s
deployment.apps/hello-world-depl   1/1     1            1           2m41s
replicaset.apps/hello-world-depl-7cb48686b   1         1         1       2m41s
```
Ein Verbindungstest sollte folgendes liefern:

```
curl -k -v https://k8s.slainte.at/
*   Trying 192.168.0.210:443...
* Connected to k8s.slainte.at (192.168.0.210) port 443 (#0)
* ALPN: offers h2
* ALPN: offers http/1.1
* TLSv1.3 (OUT), TLS handshake, Client hello (1):
* TLSv1.3 (IN), TLS handshake, Server hello (2):
* TLSv1.3 (IN), TLS handshake, Encrypted Extensions (8):
* TLSv1.3 (IN), TLS handshake, Certificate (11):
* TLSv1.3 (IN), TLS handshake, CERT verify (15):
* TLSv1.3 (IN), TLS handshake, Finished (20):
* TLSv1.3 (OUT), TLS change cipher, Change cipher spec (1):
* TLSv1.3 (OUT), TLS handshake, Finished (20):
* SSL connection using TLSv1.3 / TLS_AES_256_GCM_SHA384
* ALPN: server accepted h2
* Server certificate:
*  subject: CN=k8s.slainte.at
*  start date: Sep  2 14:08:43 2022 GMT
*  expire date: Dec  1 14:08:42 2022 GMT
*  issuer: C=US; O=Let's Encrypt; CN=R3
*  SSL certificate verify result: unable to get local issuer certificate (20), continuing anyway.
* Using HTTP2, server supports multiplexing
* Copying HTTP/2 data in stream buffer to connection buffer after upgrade: len=0
* h2h3 [:method: GET]
* h2h3 [:path: /]
* h2h3 [:scheme: https]
* h2h3 [:authority: k8s.slainte.at]
* h2h3 [user-agent: curl/7.84.0]
* h2h3 [accept: */*]
* Using Stream ID: 1 (easy handle 0x558b0c8b60c0)
> GET / HTTP/2
> Host: k8s.slainte.at
> user-agent: curl/7.84.0
> accept: */*
> 
* TLSv1.3 (IN), TLS handshake, Newsession Ticket (4):
* TLSv1.3 (IN), TLS handshake, Newsession Ticket (4):
* old SSL session ID is stale, removing
* Connection state changed (MAX_CONCURRENT_STREAMS == 128)!
< HTTP/2 200 
< date: Fri, 02 Sep 2022 15:40:47 GMT
< content-type: text/html; charset=utf-8
< strict-transport-security: max-age=15724800; includeSubDomains
< 
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN"><html><head>	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>	<title></title>	<meta name="generator" content="LibreOffice 7.1.6.2 (Linux)"/>	<meta name="author" content="Alfred Sabitzer"/>	<meta name="created" content="2021-10-07T16:33:50.696105947"/>	<meta name="changedby" content="Alfred Sabitzer"/>	<meta name="changed" content="2021-10-07T16:36:08.816466992"/>	<style type="text/css">		@page { size: 21cm 29.7cm; margin: 2cm }		p { margin-bottom: 0.25cm; line-height: 115%; background: transparent }		td p { orphans: 0; widows: 0; background: transparent }	</style></head><body lang="de-AT" link="#000080" vlink="#800000" dir="ltr"><p style="margin-bottom: 0cm; line-height: 100%"><p style="margin-bottom: 0cm; line-height: 100%"><br/></p><table width="100%" cellpadding="4" cellspacing="0">	<col width="64*"/>	<col width="64*"/>	<col width="64*"/>	<col width="64*"/>	<tr valign="top">		<td width="25%" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0.1cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm"><p>			OSENVIRONMENT</p>		</td><td width="25%" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0.1cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm"><p>			DISKUSAGE</p>		</td>		<td width="25%" style="border-top: 1px solid #000000; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0.1cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm"><p>			HOSTINFO</p>		</td>		<td width="25%" style="border: 1px solid #000000; padding: 0.1cm"><p>			MEMINFO</p>	</td>	</tr>	<tr valign="top">		<td width="25%" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm"><p>		PATH=>/go/bin:/usr/local/go/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin<br>HOSTNAME=>hello-world-depl-7cb48686b-lrk6x<br>GOLANG_VERSION=>1.16.15<br>GOPATH=>/go<br>image=>hello-world<br>tag=>latest<br>KUBERNETES_SERVICE_PORT=>443<br>KUBERNETES_SERVICE_PORT_HTTPS=>443<br>KUBERNETES_PORT_443_TCP_PORT=>443<br>HELLO_WORLD_SVC_PORT_80_TCP_PORT=>80<br>KUBERNETES_SERVICE_HOST=>10.152.183.1<br>HELLO_WORLD_SVC_SERVICE_PORT=>80<br>HELLO_WORLD_SVC_PORT_80_TCP=>tcp://10.152.183.196:80<br>HELLO_WORLD_SVC_PORT_80_TCP_PROTO=>tcp<br>KUBERNETES_PORT=>tcp://10.152.183.1:443<br>KUBERNETES_PORT_443_TCP=>tcp://10.152.183.1:443<br>KUBERNETES_PORT_443_TCP_ADDR=>10.152.183.1<br>HELLO_WORLD_SVC_SERVICE_PORT_HELLO_WORLD=>80<br>HELLO_WORLD_SVC_PORT=>tcp://10.152.183.196:80<br>HELLO_WORLD_SVC_PORT_80_TCP_ADDR=>10.152.183.196<br>KUBERNETES_PORT_443_TCP_PROTO=>tcp<br>HELLO_WORLD_SVC_SERVICE_HOST=>10.152.183.196<br>HOME=>/root<br>      </p>		</td>		<td width="25%" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm"><p>			Pfad:/<br>FSTYPE:<br>Total disk space: 58.2 GB<br>Free disk space: 42.3 GB<br>Used disk space: 13.5 GB<br>Used GB Prozent:24.2<br>Used Inodes:320360<br>Used Inodes Prozent:8.5			</p>		</td>		<td width="25%" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm"><p>			Hostname: hello-world-depl-7cb48686b-lrk6x<br>OS: linux<br>Platform: alpine<br>Host ID(uuid): 41c32620-9b35-4c8e-b074-9769b0a41aec<br>Uptime (sec): 39381<br>Number of processes running: 1</p>		</td><td width="25%" style="border-top: none; border-bottom: 1px solid #000000; border-left: 1px solid #000000; border-right: 1px solid #000000; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0.1cm"><p>			OS : linux<br>Total memory:  * Connection #0 to host k8s.slainte.at left intact
 7.6 GB<br>Free memory:   0.9 GB<br>Used memory:   3.8 GB<br>Percentage used memory: 50.30</p>		</td>	</tr>	</table><p style="margin-bottom: 0cm; line-height: 100%"><br/></p><p style="margin-bottom: 0cm; line-height: 100%">Es ist <span style="background: #c0c0c0"><sdfield type=DATETIME sdval="44476,6908896088" sdnum="3079;3079;T. MMMM JJJJ">2022-09-02 17:40:47 Friday</sdfield></span></p></body></html>
```

Somit funktioniert alles!