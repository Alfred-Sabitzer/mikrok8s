#!/bin/bash
############################################################################################
#
# Erzeugen der Credentials für das pullen der Images aus der Dockerregistry.
# Dieses Credential wird in jedem Namespace gebraucht.
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
docker_registry="docker.k8s.slainte.at"
docker_username=$(cat ${HOME}/.password/docker_username.txt)
docker_password=$(cat ${HOME}/.password/docker_password.txt)
docker_email=$(cat ${HOME}/.password/docker_email.txt)
cat <<EOF > dockerconfigjson.yaml
#
# Das sind die Credentials, damit das Image-Pull aus dem privaten Repository funktioniert.
# Der Eintrag wird pro Namespace gebraucht.
# Erzeugt wird das initial mit
#
# kubectl -n <namespace> create secret docker-registry dockerconfigjson --docker-server=docker.k8s.slainte.at --docker-username=<username> --docker-password=<password>--docker-email=<email>
#
---
EOF
#
kubectl -n admin delete secret dockerconfigjson
kubectl -n admin create secret docker-registry dockerconfigjson \
	--docker-server=${docker_registry} \
	--docker-username=${docker_username} \
	--docker-password=${docker_password} \
	--docker-email=${docker_email}
kubectl -n admin get secret dockerconfigjson -o yaml > /tmp/dockerconfigjson.yaml

# Iterate the string array using for loop
for val in $(kubectl get namespaces | grep -v NAME | awk '{print $1}' ); do
   echo $val
   cat /tmp/dockerconfigjson.yaml | grep -v "creationTimestamp:" | \
      grep -v "resourceVersion:" | \
      grep -v "selfLink:" | \
      grep -v "uid:" | \
      sed 's/namespace: admin/namespace: '${val}'/g' >> dockerconfigjson.yaml
   echo "---" >> dockerconfigjson.yaml
done
#
kubectl delete -f dockerconfigjson.yaml
kubectl apply -f dockerconfigjson.yaml
rm -f /tmp/dockerconfigjson.yaml
#