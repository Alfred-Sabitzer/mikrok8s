#!/bin/bash
############################################################################################
#
# Manipulieren der microk8s.registry
# https://gist.github.com/Kevinrob/4c7f1e5dbf6ce4e94d6ba2bfeff37aeb
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
docker_registry="https://docker.k8s.slainte.at"
docker_username=$(cat ${HOME}/.password/docker_username.txt)
docker_password=$(cat ${HOME}/.password/docker_password.txt)

# Prüfung ob gesetzt, sonst liefert die Shell hier eine Fehlermeldung
string="${1}"
#echo "${string}"
prefix=":"
suffix=""
xrepo=${string%"$prefix"*}
#echo "${xrepo}"
xtag=${string#"$xrepo$prefix"}
#echo "${xtag}"
xtag=${xtag%"$suffix"*}
#echo "${xtag}"

tags=$(curl -u ${docker_username}:${docker_password} -sSL "${docker_registry}/v2/${xrepo}/tags/list" | jq -r '.tags[]')
for tag in $tags; do
	if [ "${tag} " = "${xtag} " ] ;
	then
		todel=$(curl -u ${docker_username}:${docker_password} -k -sSL -I \
          		          -H "Accept: application/vnd.docker.distribution.manifest.v2+json" \
          		          "${docker_registry}/v2/${xrepo}/manifests/${xtag}" \
          		          | awk '$1 == "docker-content-digest:" { print $2 }' \
          		          | tr -d $'\r' )
	  echo "Lösche      "$xrepo:$tag" "${todel}
	  #echo curl -u ${docker_username}:${docker_password} -k -v -sSL -X DELETE "${docker_registry}/v2/${xrepo}/manifests/${todel} "
	  curl -u ${docker_username}:${docker_password} -k -v -sSL -X DELETE "${docker_registry}/v2/${xrepo}/manifests/${todel}"
	fi
done
#
exit