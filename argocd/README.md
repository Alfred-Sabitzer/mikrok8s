# Argo-CD

Installation der https://argo-cd.readthedocs.io 
# Installation

Siehe https://cheppers.com/post/how-install-argocd-kubernetes-cluster/
und https://argo-cd.readthedocs.io/en/stable/operator-manual/installation/

Der Namespace wird durch das yaml erzeugt.

```bash
kubectl apply -f argocd-namespace.yaml
```

Das Projekt und die Applikation werden in den einzelnen yamls definiert.
