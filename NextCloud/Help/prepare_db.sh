#!/bin/bash
############################################################################################
#
# Vorbereiten der Datenbank für die Installation der Nextcloud
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition

secret=$(mktemp /tmp/do_pg.XXXXXX)
cat <<EOF > ${secret}
#!/bin/sh
file=\$(mktemp /tmp/do_pg.XXXXXX)
cat <<XXX > \${file}
-- Löschen der Datenbank
DROP DATABASE oc_nextcloud;
-- Löschen der Rolle
drop role oc_nextcloud;
XXX
psql -e -f \${file}
rm  \${file}
EOF

kubectl exec -i --namespace=slainte postgresdb-state-0 -c postgresdb "--" sh < ${secret}
rm -f ${secret}