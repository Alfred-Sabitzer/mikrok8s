#!/bin/sh
# Connect in den Nextcloud Container
mypod=$(kubectl get pod -n slainte | grep -i nextcloud-cron | awk '{print $1 }')
kubectl exec -i -t -n slainte ${mypod} -c nextcloud-cron "--" sh -c "clear; (bash || ash || sh)"
