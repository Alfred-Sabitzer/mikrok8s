#!/bin/sh
# Erzeugen eines Tokens für die Nextcloud
# Das ist ein Muster
#
vi /etc/passwd
# Sicherstellen, dass sich der www-data auch einlogen darf
su -p www-data
cd /var/www/html
# Passwort aus dem Keepassxc
TOKEN="RLBqGmI95hmDaSMXAHSipBpefkHo3bRY"
# Set token (using the occ console application)
php occ config:app:set serverinfo token --value "$TOKEN"
# erwartetes Ergebnis
#----------- Config value token for app serverinfo set to RLBqGmI95hmDaSMXAHSipBpefkHo3bRY
#