#!/bin/sh
# Connect in den Nextcloud Exporter
mypod=$(kubectl get pod -n slainte | grep -i nextcloud-exp | awk '{print $1 }')
kubectl exec -i -t -n slainte ${mypod} -c nextcloud-exp "--" sh -c "clear; (bash || ash || sh)"
