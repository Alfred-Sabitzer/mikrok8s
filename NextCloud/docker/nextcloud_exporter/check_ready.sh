#!/bin/sh
############################################################################################
#
# Prüfen, ob der Nextcloud-Exporter bereit ist
#
############################################################################################"
#
app=$(ps -ef | grep -i nextcloud-exporter | grep -v grep)
if [ "${app}x"  == "x"  ] ; then # nextcloud-exporter läuft noch nicht
  exit 1
fi
#