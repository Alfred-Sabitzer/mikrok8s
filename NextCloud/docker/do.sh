#!/bin/bash
############################################################################################
#
# Bauen und deployen - Nextcloud und Nginx und Exporter
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
git pull --ff-only
cd ./nextcloud_app
./do.sh
cd ../nextcloud_nginx
./do.sh
cd ../nextcloud_exporter
./do.sh
