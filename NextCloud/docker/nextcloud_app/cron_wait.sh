#!/bin/sh
# Warten mit dem Cron, bis wir beginnen können
#set -ux
set -u
lockfile="/var/www/html/*.lock"
# Warte bis die Initialisierung sicher gestartet hat
date
sleep 30s
lock=$(ls ${lockfile})
lockret=$(echo $?)
echo $lock
echo $lockret

count=0
limit=10

if [ ${lockret} -eq 0  ] ; then
    until [ ${lockret} -ne 0 ] || [ "$count" -gt "$limit" ]
    do
        count=$((count+1))
        wait=$((count*10))
        echo "Another process is initializing Nextcloud. Waiting $wait seconds..."
        date
        sleep $wait
        lock=$(ls ${lockfile})
        lockret=$(echo $?)
        echo $lock
        echo $lockret
    done
    if [ "$count" -gt "$limit" ]; then
        echo "Timeout while waiting for an ongoing initialization"
        exit 1
    fi
    echo "The other process is done, assuming complete initialization"
fi
date
# Jetzt kommt der Originalteil
/cron.sh