#!/bin/sh
############################################################################################
#
# Konfiguration und Update der Nextcloud
# https://docs.nextcloud.com/server/latest/admin_manual/configuration_server/occ_command.html
#
############################################################################################"
#
cd /var/www/html
pwd
# Warten bis die Applikation läuft
app=$(ps -ef | grep -i 'php-fpm: master process' | grep -v grep)
while [ "${app}x"  == "x"  ]
do
	echo "`date` [`hostname`] warte auf den Masterprozess "
	sleep $((300 + RANDOM % 11));
  app=$(ps -ef | grep -i 'php-fpm: master process' | grep -v grep)
done

#/var/www/html $ php occ
#Nextcloud 24.0.6
#
#Usage:
#  command [options] [arguments]
#
#Options:
#  -h, --help            Display this help message
#  -q, --quiet           Do not output any message
#  -V, --version         Display this application version
#      --ansi            Force ANSI output
#      --no-ansi         Disable ANSI output
#  -n, --no-interaction  Do not ask any interactive question
#      --no-warnings     Skip global warnings, show command output only
#  -v|vv|vvv, --verbose  Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug
#
#Available commands:
#  check                                  check dependencies of the server environment
#  help                                   Display help for a command
#  list                                   List commands
#  status                                 show some status information
#  upgrade                                run upgrade routines after installation of a new release. The release has to be installed before.
# activity
#  activity:send-mails                    Sends the activity notification mails
# app
#  app:check-code                         check code to be compliant
#  app:disable                            disable an app
#  app:enable                             enable an app
#  app:getpath                            Get an absolute path to the app directory
#  app:install                            install an app
#  app:list                               List all available apps
#  app:remove                             remove an app
#  app:update                             update an app or all apps
# background
#  background:ajax                        Use ajax to run background jobs
#  background:cron                        Use cron to run background jobs
#  background:webcron                     Use webcron to run background jobs
# background-job
#  background-job:execute                 Execute a single background job manually
# broadcast
#  broadcast:test                         test the SSE broadcaster
# circles
#  circles:check                          Checking your configuration
#  circles:maintenance                    Clean stuff, keeps the app running
#  circles:manage:config                  edit config/type of a Circle
#  circles:manage:create                  create a new circle
#  circles:manage:destroy                 destroy a circle by its ID
#  circles:manage:details                 get details about a circle by its ID
#  circles:manage:edit                    edit displayName or description of a Circle
#  circles:manage:join                    emulate a user joining a Circle
#  circles:manage:leave                   simulate a user joining a Circle
#  circles:manage:list                    listing current circles
#  circles:manage:setting                 edit setting for a Circle
#  circles:members:add                    Add a member to a Circle
#  circles:members:details                get details about a member by its ID
#  circles:members:level                  Change the level of a member from a Circle
#  circles:members:list                   listing Members from a Circle
#  circles:members:remove                 remove a member from a circle
#  circles:members:search                 Change the level of a member from a Circle
#  circles:memberships                    index and display memberships for local and federated users
#  circles:remote                         remote features
#  circles:shares:files                   listing shares files
#  circles:sync                           Sync Circles and Members
#  circles:test                           testing some features
# config
#  config:app:delete                      Delete an app config value
#  config:app:get                         Get an app config value
#  config:app:set                         Set an app config value
#  config:import                          Import a list of configs
#  config:list                            List all configs
#  config:system:delete                   Delete a system config value
#  config:system:get                      Get a system config value
#  config:system:set                      Set a system config value
# dav
#  dav:create-addressbook                 Create a dav addressbook
#  dav:create-calendar                    Create a dav calendar
#  dav:delete-calendar                    Delete a dav calendar
#  dav:list-calendars                     List all calendars of a user
#  dav:move-calendar                      Move a calendar from an user to another
#  dav:remove-invalid-shares              Remove invalid dav shares
#  dav:retention:clean-up
#  dav:send-event-reminders               Sends event reminders
#  dav:sync-birthday-calendar             Synchronizes the birthday calendar
#  dav:sync-system-addressbook            Synchronizes users to the system addressbook
# db
#  db:add-missing-columns                 Add missing optional columns to the database tables
#  db:add-missing-indices                 Add missing indices to the database tables
#  db:add-missing-primary-keys            Add missing primary keys to the database tables
#  db:convert-filecache-bigint            Convert the ID columns of the filecache to BigInt
#  db:convert-mysql-charset               Convert charset of MySQL/MariaDB to use utf8mb4
#  db:convert-type                        Convert the Nextcloud database to the newly configured one
# encryption
#  encryption:change-key-storage-root     Change key storage root
#  encryption:decrypt-all                 Disable server-side encryption and decrypt all files
#  encryption:disable                     Disable encryption
#  encryption:enable                      Enable encryption
#  encryption:encrypt-all                 Encrypt all files for all users
#  encryption:list-modules                List all available encryption modules
#  encryption:migrate-key-storage-format  Migrate the format of the keystorage to a newer format
#  encryption:set-default-module          Set the encryption default module
#  encryption:show-key-storage-root       Show current key storage root
#  encryption:status                      Lists the current status of encryption
# federation
#  federation:sync-addressbooks           Synchronizes addressbooks of all federated clouds
# files
#  files:cleanup                          cleanup filecache
#  files:recommendations:recommend
#  files:repair-tree                      Try and repair malformed filesystem tree structures
#  files:scan                             rescan filesystem
#  files:scan-app-data                    rescan the AppData folder
#  files:transfer-ownership               All files and folders are moved to another user - outgoing shares and incoming user file shares (optionally) are moved as well.
# group
#  group:add                              Add a group
#  group:adduser                          add a user to a group
#  group:delete                           Remove a group
#  group:info                             Show information about a group
#  group:list                             list configured groups
#  group:removeuser                       remove a user from a group
# integrity
#  integrity:check-app                    Check integrity of an app using a signature.
#  integrity:check-core                   Check integrity of core code using a signature.
#  integrity:sign-app                     Signs an app using a private key.
#  integrity:sign-core                    Sign core using a private key.
# l10n
#  l10n:createjs                          Create javascript translation files for a given app
# log
#  log:file                               manipulate logging backend
#  log:manage                             manage logging configuration
#  log:tail                               Tail the nextcloud logfile
#  log:watch                              Watch the nextcloud logfile
# maintenance
#  maintenance:data-fingerprint           update the systems data-fingerprint after a backup is restored
#  maintenance:mimetype:update-db         Update database mimetypes and update filecache
#  maintenance:mimetype:update-js         Update mimetypelist.js
#  maintenance:mode                       set maintenance mode
#  maintenance:repair                     repair this installation
#  maintenance:theme:update               Apply custom theme changes
#  maintenance:update:htaccess            Updates the .htaccess file
# notification
#  notification:generate                  Generate a notification for the given user
#  notification:test-push                 Generate a notification for the given user
# preview
#  preview:repair                         distributes the existing previews into subfolders
#  preview:reset-rendered-texts           Deletes all generated avatars and previews of text and md files
# security
#  security:bruteforce:reset              resets bruteforce attemps for given IP address
#  security:certificates                  list trusted certificates
#  security:certificates:import           import trusted certificate in PEM format
#  security:certificates:remove           remove trusted certificate
# serverinfo
#  serverinfo:update-storage-statistics   Triggers an update of the counts related to storages used in serverinfo
# sharing
#  sharing:cleanup-remote-storages        Cleanup shared storage entries that have no matching entry in the shares_external table
#  sharing:expiration-notification        Notify share initiators when a share will expire the next day.
# tag
#  tag:add                                Add new tag
#  tag:delete                             delete a tag
#  tag:edit                               edit tag attributes
#  tag:list                               list tags
# text
#  text:reset                             Reset a text document
# theming
#  theming:config                         Set theming app config values
# trashbin
#  trashbin:cleanup                       Remove deleted files
#  trashbin:expire                        Expires the users trashbin
#  trashbin:restore                       Restore all deleted files
#  trashbin:size                          Configure the target trashbin size
# twofactorauth
#  twofactorauth:cleanup                  Clean up the two-factor user-provider association of an uninstalled/removed provider
#  twofactorauth:disable                  Disable two-factor authentication for a user
#  twofactorauth:enable                   Enable two-factor authentication for a user
#  twofactorauth:enforce                  Enabled/disable enforced two-factor authentication
#  twofactorauth:state                    Get the two-factor authentication (2FA) state of a user
# update
#  update:check                           Check for server and app updates
# user
#  user:add                               adds a user
#  user:add-app-password                  Add app password for the named user
#  user:delete                            deletes the specified user
#  user:disable                           disables the specified user
#  user:enable                            enables the specified user
#  user:info                              show user info
#  user:lastseen                          shows when the user was logged in last time
#  user:list                              list configured users
#  user:report                            shows how many users have access
#  user:resetpassword                     Resets the password of the named user
#  user:setting                           Read and modify user settings
# versions
#  versions:cleanup                       Delete versions
#  versions:expire                        Expires the users file versions
# workflows
#  workflows:list                         Lists configured workflows

#/var/www/html $ php occ  config:list
#{
#    "system": {
#        "memcache.local": "\\OC\\Memcache\\APCu",
#        "apps_paths": [
#            {
#                "path": "\/var\/www\/html\/apps",
#                "url": "\/apps",
#                "writable": false
#            },
#            {
#                "path": "\/var\/www\/html\/custom_apps",
#                "url": "\/custom_apps",
#                "writable": true
#            }
#        ],
#        "memcache.distributed": "\\OC\\Memcache\\Redis",
#        "memcache.locking": "\\OC\\Memcache\\Redis",
#        "redis": {
#            "host": "***REMOVED SENSITIVE VALUE***",
#            "password": "***REMOVED SENSITIVE VALUE***",
#            "port": 6379
#        },
#        "passwordsalt": "***REMOVED SENSITIVE VALUE***",
#        "secret": "***REMOVED SENSITIVE VALUE***",
#        "trusted_domains": [
#            "localhost",
#            "www.nextcloud.k8s.slainte.at",
#            "10.0.0.0\/8",
#            "192.0.0.0\/8"
#        ],
#        "datadirectory": "***REMOVED SENSITIVE VALUE***",
#        "dbtype": "pgsql",
#        "version": "24.0.6.1",
#        "overwrite.cli.url": "http:\/\/localhost",
#        "dbname": "***REMOVED SENSITIVE VALUE***",
#        "dbhost": "***REMOVED SENSITIVE VALUE***",
#        "dbport": "",
#        "dbtableprefix": "oc_",
#        "dbuser": "***REMOVED SENSITIVE VALUE***",
#        "dbpassword": "***REMOVED SENSITIVE VALUE***",
#        "installed": true,
#        "instanceid": "***REMOVED SENSITIVE VALUE***"
#    },
#    "apps": {
#        "accessibility": {
#            "installed_version": "1.10.0",
#            "types": "",
#            "enabled": "yes"
#        },
#        "activity": {
#            "installed_version": "2.16.0",
#            "types": "filesystem",
#            "enabled": "yes"
#        },
#        "backgroundjob": {
#            "lastjob": "1"
#        },
#        "circles": {
#            "installed_version": "24.0.1",
#            "types": "filesystem,dav",
#            "enabled": "yes",
#            "loopback_tmp_scheme": "http",
#            "maintenance_run": "0",
#            "maintenance_update": "{\"3\":1668853208,\"2\":1668853208,\"1\":1668853505}"
#        },
#        "cloud_federation_api": {
#            "installed_version": "1.7.0",
#            "types": "filesystem",
#            "enabled": "yes"
#        },
#        "comments": {
#            "installed_version": "1.14.0",
#            "types": "logging",
#            "enabled": "yes"
#        },
#        "contactsinteraction": {
#            "installed_version": "1.5.0",
#            "types": "dav",
#            "enabled": "yes"
#        },
#        "core": {
#            "installedat": "1668852883.6206",
#            "lastupdatedat": "1668852883.6707",
#            "vendor": "nextcloud",
#            "public_webdav": "dav\/appinfo\/v1\/publicwebdav.php",
#            "public_files": "files_sharing\/public.php",
#            "backgroundjobs_mode": "cron",
#            "lastcron": "1668853506"
#        },
#        "dashboard": {
#            "installed_version": "7.4.0",
#            "types": "",
#            "enabled": "yes"
#        },
#        "dav": {
#            "installed_version": "1.22.0",
#            "types": "filesystem",
#            "enabled": "yes"
#        },
#        "federatedfilesharing": {
#            "installed_version": "1.14.0",
#            "types": "",
#            "enabled": "yes"
#        },
#        "federation": {
#            "installed_version": "1.14.0",
#            "types": "authentication",
#            "enabled": "yes"
#        },
#        "files": {
#            "installed_version": "1.19.0",
#            "types": "filesystem",
#            "enabled": "yes"
#        },
#        "files_pdfviewer": {
#            "installed_version": "2.5.0",
#            "types": "",
#            "enabled": "yes"
#        },
#        "files_rightclick": {
#            "installed_version": "1.3.0",
#            "types": "",
#            "enabled": "yes"
#        },
#        "files_sharing": {
#            "installed_version": "1.16.2",
#            "types": "filesystem",
#            "enabled": "yes"
#        },
#        "files_trashbin": {
#            "installed_version": "1.14.0",
#            "types": "filesystem,dav",
#            "enabled": "yes"
#        },
#        "files_versions": {
#            "installed_version": "1.17.0",
#            "types": "filesystem,dav",
#            "enabled": "yes"
#        },
#        "files_videoplayer": {
#            "installed_version": "1.13.0",
#            "types": "",
#            "enabled": "yes"
#        },
#        "firstrunwizard": {
#            "installed_version": "2.13.0",
#            "types": "logging",
#            "enabled": "yes"
#        },
#        "logreader": {
#            "installed_version": "2.9.0",
#            "types": "",
#            "enabled": "yes"
#        },
#        "lookup_server_connector": {
#            "installed_version": "1.12.0",
#            "types": "authentication",
#            "enabled": "yes"
#        },
#        "nextcloud_announcements": {
#            "installed_version": "1.13.0",
#            "types": "logging",
#            "enabled": "yes",
#            "pub_date": "Thu, 24 Oct 2019 00:00:00 +0200"
#        },
#        "notifications": {
#            "installed_version": "2.12.1",
#            "types": "logging",
#            "enabled": "yes"
#        },
#        "oauth2": {
#            "installed_version": "1.12.0",
#            "types": "authentication",
#            "enabled": "yes"
#        },
#        "password_policy": {
#            "installed_version": "1.14.0",
#            "types": "authentication",
#            "enabled": "yes"
#        },
#        "photos": {
#            "installed_version": "1.6.0",
#            "types": "",
#            "enabled": "yes"
#        },
#        "privacy": {
#            "installed_version": "1.8.0",
#            "types": "",
#            "enabled": "yes"
#        },
#        "provisioning_api": {
#            "installed_version": "1.14.0",
#            "types": "prevent_group_restriction",
#            "enabled": "yes"
#        },
#        "recommendations": {
#            "installed_version": "1.3.0",
#            "types": "",
#            "enabled": "yes"
#        },
#        "serverinfo": {
#            "installed_version": "1.14.0",
#            "types": "",
#            "enabled": "yes",
#            "cached_count_filecache": "37",
#            "cached_count_storages": "2"
#        },
#        "settings": {
#            "installed_version": "1.6.0",
#            "types": "",
#            "enabled": "yes"
#        },
#        "sharebymail": {
#            "installed_version": "1.14.0",
#            "types": "filesystem",
#            "enabled": "yes"
#        },
#        "support": {
#            "installed_version": "1.7.0",
#            "types": "session",
#            "enabled": "yes"
#        },
#        "survey_client": {
#            "installed_version": "1.12.0",
#            "types": "",
#            "enabled": "yes"
#        },
#        "systemtags": {
#            "installed_version": "1.14.0",
#            "types": "logging",
#            "enabled": "yes"
#        },
#        "text": {
#            "installed_version": "3.5.1",
#            "types": "dav",
#            "enabled": "yes"
#        },
#        "theming": {
#            "installed_version": "1.15.0",
#            "types": "logging",
#            "enabled": "yes"
#        },
#        "twofactor_backupcodes": {
#            "installed_version": "1.13.0",
#            "types": "",
#            "enabled": "yes"
#        },
#        "updatenotification": {
#            "installed_version": "1.14.0",
#            "types": "",
#            "enabled": "yes"
#        },
#        "user_status": {
#            "installed_version": "1.4.0",
#            "types": "",
#            "enabled": "yes"
#        },
#        "viewer": {
#            "installed_version": "1.8.0",
#            "types": "",
#            "enabled": "yes"
#        },
#        "weather_status": {
#            "installed_version": "1.4.0",
#            "types": "",
#            "enabled": "yes"
#        },
#        "workflowengine": {
#            "installed_version": "2.6.0",
#            "types": "filesystem",
#            "enabled": "yes"
#        }
#    }
#}

echo "Starten der Konfiguration"
cd /var/www/html
su -s /bin/sh www-data -c "php occ config:system:set redis host --value=${REDIS_HOST}"
su -s /bin/sh www-data -c "php occ config:system:set redis port --value=${REDIS_HOST_PORT}"
su -s /bin/sh www-data -c "php occ config:system:set redis dbindex --value=3"
su -s /bin/sh www-data -c "php occ config:system:set redis password --value=${REDIS_HOST_PASSWORD}"
su -s /bin/sh www-data -c "php occ config:system:set redis timeout --value=1.5"

su -s /bin/sh www-data -c "php occ config:system:set dbname --value=${POSTGRES_DB}"
su -s /bin/sh www-data -c "php occ config:system:set dbhost --value=${POSTGRES_HOST}"
su -s /bin/sh www-data -c "php occ config:system:set dbport --value=5432"
su -s /bin/sh www-data -c "php occ config:system:set dbtableprefix --value=oc_"
# Dann bricht die DB-Verbindung
#su -s /bin/sh www-data -c "php occ config:system:set dbuser --value=${POSTGRES_DB}"
#su -s /bin/sh www-data -c "php occ config:system:set dbpassword --value=${POSTGRES_PASSWORD}"

# Update des Master-Passworte
export OC_PASS=${NEXTCLOUD_ADMIN_PASSWORD}
su -s /bin/sh www-data -c "php occ user:resetpassword --password-from-env ${NEXTCLOUD_ADMIN_USER}"

# Set token (using the occ console application)
su -s /bin/sh www-data -c "php occ config:app:set serverinfo token --value ${NEXTCLOUD_TOKEN}"
#
su -s /bin/sh www-data -c "php occ trashbin:cleanup --all-users"
su -s /bin/sh www-data -c "php occ update:check"
#su -s /bin/sh www-data -c "php occ update"
#su -s /bin/sh www-data -c "php occ upgrade"
echo "Nextcloud ist konfiguriert"
# Jetzt ist Nextcloud am neuesten Stand