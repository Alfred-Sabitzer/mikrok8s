#!/bin/sh
# Disable exit on non 0
set +e
echo "############################################################################################"
echo "#"
echo "# Starten der Nextcloud mit den richtigen Werten."
echo "#"
echo "############################################################################################"
#
#export
#
#sed -i 's|set -eu|set -eux|' /entrypoint.sh
chmod 0770 ${NEXTCLOUD_DATA_DIR}
chown www-data:www-data ${NEXTCLOUD_DATA_DIR}
# Wird als Daemon gestartet
/entrypoint.sh php-fpm --fpm-config /usr/local/etc/php/php-fpm.conf --daemonize=yes &
/Nextcloud_Config.sh
# Sicherstellen, dass der Prozess oben bleibt
/dummy.sh
#