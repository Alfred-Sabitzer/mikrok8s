#!/bin/bash
############################################################################################
#
# Verwalten der Passworte
#
# Name pgadmin
# ./do_pg.sh pg_admin "slainte" "postgresdb-svc" "cGdhZG1pbg==" "SnBHVUNlZnB4VXAySkhiR2ZhTGhScnRibkZLMnFwNzFOR3I1TEVZcA=="
# Name NextCloudPostgres
# ./do_pg.sh pg_user "slainte" "postgresdb-svc" "TmV4dENsb3VkUG9zdGdyZXM=" "RmtQT3VsaHZUWklqUjFJaHFUWVlINE9FS3I5N0Jp"
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
# Überprüfen der Parameter
if [ "${1}" = "" ] || [ "${2}" = "" ] || [ "${3}" = "" ] || [ "${4}" = "" ]  || [ "${5}" = "" ] ;
then
	echo "Usage: $0 Methode Namespace Service User Passwort "
	echo "eg: $0	pg_user \"slainte\" \"pgadmin-svc\" \"cGdhZG1pbg==\" \"SnBHVUNlZnB4VXAySkhiR2ZhTGhScnRibkZLMnFwNzFOR3I1TEVZcA==\" "
	exit -1
fi
shopt -o -s nounset #-No Variables without definition
#
secret=$(mktemp /tmp/do_pg.XXXXXX)

communicate ()
{
  kubectl exec -i --namespace=${namespace} ${podname} -c postgresdb -- bash < ${secret}
  echo "done"
}

pg_admin ()
{
#echo  "pg_user "${namespace}" "${service}" "${user}" "${passwort}" ;;"

cat <<EOF > ${secret}
#!/bin/sh
file=\$(mktemp /tmp/do_pg.XXXXXX)
cat <<XXX > \${file}
-- Anlegen des Users
select create_superuser('${3}','${4}');
select set_password('${3}','${4}');
-- Anlegen der Datenbank
select create_database('${3}','${3}');
GRANT ALL ON DATABASE "${3}" TO "${3}";
XXX
psql -e -f \${file}
rm  \${file}
EOF
communicate $1 $2 $3
}

pg_user ()
{
#echo  "pg_user "${namespace}" "${service}" "${user}" "${passwort}" ;;"
cat <<EOF > ${secret}
#!/bin/sh
file=\$(mktemp /tmp/do_pg.XXXXXX)
cat <<XXX > \${file}
select create_user('${3}','$4}');
select set_password('${3}','${4}');
XXX
psql -e -f \${file}
rm  \${file}
EOF
communicate $1 $2 $3
}
# ./do_pg.sh pg_admin "slainte" "postgresdb-svc" "cGdhZG1pbg==" "SnBHVUNlZnB4VXAySkhiR2ZhTGhScnRibkZLMnFwNzFOR3I1TEVZcA=="
methode=$1
namespace=$2
service=$3
user=$(echo $4 | base64 --decode)
passwort=$(echo $5 | base64 --decode)
#./do_pg.sh pg_user "slainte" "admin/pgadmin-svc" "am9vbWxh" "N2VWcWJ0dlZ4dnY5YlpMNUhxT2JMMnN2WXFEVGpQZ3JRRTJvRVE1dA=="
#echo "Methode: ${methode} Namespace: ${namespace} Service: ${service} User: ${user} Passwort: ${passwort}"

serviceIP=$(kubectl get -n ${namespace} service | grep -i ${service} | awk '{print $3 }')
if [ "${serviceIP} " = " " ] ;
then
	echo "FEHLER: ${service} im Namespace ${namespace} kann nicht gefunden werden! ${serviceIP}"
	exit -1
fi

endpoint=$(kubectl describe service -n ${namespace} ${service} | grep -i endpoints: | awk '{print $2 }')
endpoint=${endpoint%:*}   # remove suffix starting with ":"
if [ "${endpoint} " = " " ] ;
then
	echo "FEHLER: ${service} im Namespace ${namespace} -> Der Endpoint kann nicht gefunden werden! ${endpoint}"
	exit -1
fi

podname=$(kubectl get pods -n ${namespace} -o wide | grep -i ${endpoint} | awk '{print $1 }')
if [ "${podname} " = " " ] ;
then
	echo "FEHLER: ${service} im Namespace ${namespace} -> Der Podname zum Endpoint ${endpoint} kann nicht gefunden werden! ${podname}"
	exit -1
fi

case ${methode} in
    pg_admin)
        pg_admin "${namespace}" "${service}" "${user}" "${passwort}" ;;
    pg_user)
        pg_user "${namespace}" "${service}" "${user}" "${passwort}" ;;
    *) ;;
esac
rm -f ${secret}
#


