#!/bin/bash
# https://greg.jeanmart.me/2020/04/13/deploy-nextcloud-on-kuberbetes--the-self-hos/
# https://github.com/nextcloud/helm/tree/master/charts/nextcloud
# https://www.tutorialworks.com/helm-cheatsheet/
#
microk8s.helm3 repo add nextcloud https://nextcloud.github.io/helm/
microk8s.helm3 repo list
microk8s.helm3 search repo nextcloud
microk8s.helm3 list
microk8s.helm3 show values nextcloud/nextcloud > nextcloud.values.yml
kubectl create namespace nextcloud
kubectl apply -f ./nextcloud_secrets.yaml
microk8s.helm3 install --namespace nextcloud -f values.yaml mync nextcloud/nextcloud
microk8s.helm3 list
exit
#
microk8s.helm3 uninstall --namespace nextcloud mync
kubectl delete all --all -n nextcloud
kubectl delete namespace nextcloud
#