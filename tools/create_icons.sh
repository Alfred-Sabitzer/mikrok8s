#!/bin/bash
# https://gist.github.com/jpoles1/b677fa078b8c88e6a7721edc5ed46eca
# Requires imagemagick be installed (sudo apt install imagemagick on ubuntu)

convert ./icon.png -resize 512x512 ./android-chrome-512x512.png
convert ./icon.png -resize 192x192 ./android-chrome-192x192.png

convert ./icon.png -resize 16x16 ./16.png
convert ./icon.png -resize 32x32 ./32.png
convert ./icon.png -resize 48x48 ./48.png

convert ./16.png ./32.png ./48.png ./favicon.ico
# Jetzt sind die Icons vorhanden