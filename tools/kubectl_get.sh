#!/bin/bash
############################################################################################
#
# Extrahieren der Yamls aus einem bestimmten Namespace
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
# Überprüfen der Parameter
if [ "${1}" = "" ]  ] ;
then
	echo "Usage: $0 Namespace "
	echo "eg: $0	nextcloud "
	exit -1
fi
namespace=${1}

do_it ()
{
what=${1}
for obj in $(kubectl get ${what} -n ${namespace}  | grep -v 'NAME' | awk '{print $1 }' ); do
  kubectl get ${what} -n ${namespace} ${obj} -o yaml > ${namespace}_${what}_${obj}.yaml
done

}
do_it 'pods'
do_it 'deployments'
do_it 'statefulsets'
do_it 'cronjobs'
do_it 'configmaps'
do_it 'secrets'
do_it 'resourcequotas'
do_it 'limitranges'
do_it 'horizontalpodautoscalers'
do_it 'poddisruptionbudgets'
do_it 'services'
do_it 'endpoints'
do_it 'ingress'
do_it 'networkpolicies'
do_it 'persistentvolumeclaims'
#
