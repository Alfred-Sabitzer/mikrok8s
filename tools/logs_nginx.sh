#!/bin/bash
############################################################################################
#
# Anzeige aller Logs der nginx-ingress-controller Daemonsets
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
kubectl logs -n ingress -f daemonsets/nginx-ingress-microk8s-controller


