#!/bin/bash
############################################################################################
#
# test
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
export POSTGRES_PASSWORD=$(cat /pgadmin/password)
export POSTGRES_USER=$(cat /pgadmin/username)
export POSTGRES_DB=$(cat /pgadmin/username)
cat <<EOF > /tmp/create_user.sql
select * from pg_catalog.pg_database;
select * from pg_catalog.pg_user;
EOF
psql -U ${POSTGRES_USER} -e -f /tmp/create_user.sql
