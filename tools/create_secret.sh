#!/bin/bash
############################################################################################
#
# Erzeugen der Kubernetes Secrets aus bereits vorhandenen Zertifikaten
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition

cat <<EOF > k8s-slainte-at-tls.yaml
#
# k8s-slainte-at-tls
#
---
#
apiVersion: v1
kind: Secret
metadata:
  name: k8s-slainte-at-tls
  namespace: slainte
type: kubernetes.io/tls
data:
EOF
echo "  tls.crt: $(cat /home/alfred/k8s/SecureRegistry/pem/server.crt | base64 -w 0)"  >> k8s-slainte-at-tls.yaml
echo "  tls.key: $(cat /home/alfred/k8s/SecureRegistry/pem/server.key | base64 -w 0)" >> k8s-slainte-at-tls.yaml
#
