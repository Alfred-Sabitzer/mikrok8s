# PVCTest

Anlage mehrerer Persistent Volume Claims

## Vorbereitung

Das SpecialImage muß abrufbereit sein

## Starten

```bash
kubectl apply -f .
```
Danach kann man sich über kubectl mit dem SpecialImage verbinden, und alle möglichen Tests mit den PVC's durchführen.
