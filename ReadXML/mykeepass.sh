#!/bin/bash
############################################################################################
#
# Extrahieren des Keepass-Files als XML und generieren der Kommandos
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
#shopt -o -s nounset #-No Variables without definition

# Überprüfen der Parameter
if [ "${1}" = "" ] ;
then
	echo "Usage: $0 keepass.kdbx "
	echo "eg: $0 /home/alfred/Nextcloud/k8s.kdbx"
	exit -1
fi
ifile=${1}
ofile="./mykeepass.xml"
if [ ! -f "${ifile}" ]; then # Das ist keine Datei
	echo "Usage: $0 keepass.kdbx "
	echo "\"$@\"" "ist keine Datei"
	exit -1
fi
keepassxc-cli export --format xml ${ifile} > ${ofile}
ls -lisa ${ofile}
echo " "
echo " Diese Ausgabedatei dient als Input für das JavaProgramm zur Erzeugung der Secrets."
echo " "
/snap/openjdk/current/jdk/bin/java -javaagent:/snap/intellij-idea-community/390/plugins/java/lib/rt/debugger-agent.jar -Dfile.encoding=UTF-8 -Dsun.stdout.encoding=UTF-8 -Dsun.stderr.encoding=UTF-8 -classpath /home/alfred/svn/slainte/trunk/Java/ReadXML/out/production/ReadXML:/snap/intellij-idea-community/390/lib/idea_rt.jar ReadKeepass.slainte.ReadKeepass
echo " "
echo " "
rm -f ${ofile}
ls -lisa mykeepass*.*
echo " "


