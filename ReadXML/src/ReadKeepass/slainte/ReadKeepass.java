/*
 *
 * sudo apt-get install apache2-utils
 *
 */

package ReadKeepass.slainte;

import java.io.*;
import java.io.File;
import java.util.UUID;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.Locale;
import java.util.Scanner;

//https://www.vogella.com/tutorials/JavaRegularExpressions/article.html

public class ReadKeepass {

    private static final String BAUTH = "/home/alfred/svn/slainte/trunk/Java/ReadXML";
    private static final String FILENAME = BAUTH+"/mykeepass.xml";
    private static final String METHODS = BAUTH+"/mykeepassmethods.sh";
    private static final String SECRETS = BAUTH+"/mykeepasssecrets.yaml";
    private static Scanner myReader;
    private static String data="";
    private static String secretNotes="";
    private static String namespace="";
    private static String namespace_comment="";
    private static String secretPassword="";
    private static String secretPasswordraw="";
    private static String secretName="";
    private static String secretUsername="";
    private static String secretUsernameraw="";
    private static String secretURL="";
    private static String secretpostgresuser="";
    private static String secretpostgresadmin="";
    private static String secretk8s="";
    private static String basicauth="";
    private static String bcryptauth="";
    private static String myval;

    private String getline() {
        return myReader.nextLine();
    }

    private static void get_history() { // Lesen bis zum Ende der History und dann wieder raus
        boolean exit_loop = false;
        do {
            data = myReader.nextLine();
            if (data.matches("(.*)</History>")) { // Jetzt sind wir am Ende
                exit_loop = true;
            }
        } while (myReader.hasNextLine() && !exit_loop);
    }

    private static void get_end(String pattern) { // Lesen bis zum Ende der Notes und dann wieder raus
        boolean exit_loop = false;
        do {
            if (data.matches("(.*)"+pattern)) { // Jetzt sind wir am Ende
                exit_loop = true;
            }
            else {
                data = myReader.nextLine();
            }
        } while (myReader.hasNextLine() && !exit_loop);
    }

    private static void get_string() { // Lesen des speziellen Strings
        boolean exit_loop = false;
        do {
            data = myReader.nextLine();
//            						<Key>Notes</Key>
            String mykey = data.replaceAll("(.*)<Key>", "");
            mykey = mykey.replaceAll("</Key>", "");
            data = myReader.nextLine();
//						<Value ProtectInMemory="True">&gt;RPgZCHmT`\pD@}S~g#/</Value>
//						<Value>testsecret</Value>
            myval = data.replaceAll("(.*)<Value>", "");
            myval = myval.replaceAll("</Value>", "");

            switch (mykey) {
                case "Notes":
                    secretNotes = myval;
                    get_end("</Value>");
                    break;
                case "Password":
                    myval = data.replaceAll("(.*)<Value ProtectInMemory=\"True\">", "");
                    myval = myval.replaceAll("</Value>", "");
                    secretPasswordraw = myval;
                    secretPassword = Base64.getEncoder().encodeToString(myval.getBytes());
                    break;
                case "Title":
                    secretName = myval;
                    break;
                case "UserName":
                    secretUsernameraw = myval;
                    secretUsername = Base64.getEncoder().encodeToString(myval.getBytes());
                    break;
                case "URL":
                    secretURL =  myval;
                    break;
                case "postgres-user":
                    secretpostgresuser =  myval;
                    break;
                case "postgres-admin":
                    secretpostgresadmin =  myval;
                    break;
                case "secret":
                    secretk8s =  myval;
                    break;
                case "basic-auth":
                    basicauth =  myval;
                    break;
                case "bcrypt-auth":
                    bcryptauth =  myval;
                    break;
                default:
            }
            data = myReader.nextLine();
            if (data.matches("(.*)</String>")) { // Jetzt sind wir am Ende
                exit_loop = true;
            }
        } while (myReader.hasNextLine() && !exit_loop);
    }

    private static void get_entry() {
        boolean exit_loop = false;
        do {
            data = myReader.nextLine();
            if (data.matches("(.*)<History>")) {
                get_history();
            }
            if (data.matches("(.*)<String>")) {
                get_string();
            }
            if (data.matches("(.*)</Entry>")) { // Jetzt sind wir am Ende, und erzeugen ein Secret.
                if (secretk8s.equals("k8s")){
                    PrintWriter printWriter;
                    try (FileWriter fileWriter = new FileWriter(SECRETS, true)) {
                        LocalDateTime now = LocalDateTime.now();
                        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                        String formatDateTime = now.format(formatter);
                        printWriter = new PrintWriter(fileWriter);
                        //printWriter.println("# " + secretNotes);
                        printWriter.println("apiVersion: v1");
                        printWriter.println("kind: Secret ");
                        printWriter.println("metadata:");
                        printWriter.println("  name: " + secretName.toLowerCase(Locale.ROOT));
                        printWriter.println("  namespace: " + namespace.toLowerCase(Locale.ROOT));
                        printWriter.println("  labels:");
                        printWriter.println("    managed-by: keepassxc");
                        printWriter.println("    generated: "+ formatDateTime);
                        printWriter.println("type: Opaque");
                        printWriter.println("data:");
                        if (! secretURL.matches("\t\t\t\t\t\t<Value/>")) {
                            printWriter.println("  apiUrl: " + Base64.getEncoder().encodeToString(secretURL.getBytes()));
                        }
                        printWriter.println("  username: " + secretUsername);
                        printWriter.println("  password: " + secretPassword);
                        printWriter.println("---");
                        printWriter.close();
                    } //Set true for append mode
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                    secretk8s="";
                }
                if (!basicauth.equals("")){
                    try {
                        // Prüfen ob es schon ein derartiges File gibt.
                        System.out.println(namespace+"_"+bcryptauth+" "+secretUsernameraw+" "+secretPasswordraw);
                        File tempFile = new File("/tmp/basic/"+namespace+"_"+basicauth);
                        boolean exists = tempFile.exists();
                        Process process;

                        if (exists) {
                            process = Runtime.getRuntime().exec("htpasswd -bm "+tempFile+" "+secretUsernameraw+" "+secretPasswordraw);
                        }
                        else {
                            process = Runtime.getRuntime().exec("htpasswd -bmc "+tempFile+" "+secretUsernameraw+" "+secretPasswordraw);
                        }
                        process.waitFor();
                        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
                        String line;
                        while ((line = reader.readLine()) != null) {
                            System.out.println(line);
                        }
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                    PrintWriter printWriter;
                    try (FileWriter fileWriter = new FileWriter(BAUTH +"/"+namespace+"_"+basicauth + ".yaml", false)) {
                        LocalDateTime now = LocalDateTime.now();
                        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                        String formatDateTime = now.format(formatter);
                        printWriter = new PrintWriter(fileWriter);
                        //printWriter.println("# " + secretNotes);
                        printWriter.println("apiVersion: v1");
                        printWriter.println("kind: Secret ");
                        printWriter.println("metadata:");
                        printWriter.println("  name: " + basicauth.toLowerCase(Locale.ROOT));
                        printWriter.println("  namespace: " + namespace.toLowerCase(Locale.ROOT));
                        printWriter.println("  labels:");
                        printWriter.println("    managed-by: keepassxc");
                        printWriter.println("    algorithm: MD5");
                        printWriter.println("    generated: "+ formatDateTime);
                        printWriter.println("type: Opaque");
                        printWriter.println("data:");
                        printWriter.println("  auth: ");
                        printWriter.println("---");
                        printWriter.close();
                    } //Set true for append mode
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                    basicauth="";
                }
                if (!bcryptauth.equals("")){ // Hier wird mit bcrypt verschlüsselt
                    try {
                        // Prüfen ob es schon ein derartiges File gibt.
                        System.out.println(namespace+"_"+bcryptauth+" "+secretUsernameraw+" "+secretPasswordraw);
                        File tempFile = new File("/tmp/basic/"+namespace+"_"+bcryptauth);
                        boolean exists = tempFile.exists();
                        Process process;
                        if (exists) {
                            process = Runtime.getRuntime().exec("htpasswd -bB "+tempFile+" "+secretUsernameraw+" "+secretPasswordraw);
                        }
                        else {
                            process = Runtime.getRuntime().exec("htpasswd -bBc "+tempFile+" "+secretUsernameraw+" "+secretPasswordraw);
                        }
                        process.waitFor();
                        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
                        String line;
                        while ((line = reader.readLine()) != null) {
                            System.out.println(line);
                        }
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                    PrintWriter printWriter;
                    try (FileWriter fileWriter = new FileWriter(BAUTH +"/"+namespace+"_"+bcryptauth + ".yaml", false)) {
                        LocalDateTime now = LocalDateTime.now();
                        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                        String formatDateTime = now.format(formatter);
                        printWriter = new PrintWriter(fileWriter);
                        //printWriter.println("# " + secretNotes);
                        printWriter.println("apiVersion: v1");
                        printWriter.println("kind: Secret ");
                        printWriter.println("metadata:");
                        printWriter.println("  name: " + bcryptauth.toLowerCase(Locale.ROOT));
                        printWriter.println("  namespace: " + namespace.toLowerCase(Locale.ROOT));
                        printWriter.println("  labels:");
                        printWriter.println("    managed-by: keepassxc");
                        printWriter.println("    algorithm: bcrypt");
                        printWriter.println("    generated: "+ formatDateTime);
                        printWriter.println("type: Opaque");
                        printWriter.println("data:");
                        printWriter.println("  auth: ");
                        printWriter.println("---");
                        printWriter.close();
                    } //Set true for append mode
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                    bcryptauth="";
                }
                if (!secretpostgresadmin.equals("")) {
                    PrintWriter printWriter;
                    try {
                        FileWriter fileWriter = new FileWriter(METHODS, true); //Set true for append mode
                        printWriter = new PrintWriter(fileWriter);
                        printWriter.println("# Name " + secretName);
                        //printWriter.println("# " + secretNotes);
                        printWriter.println("./do_pg.sh pg_admin \"" + namespace.toLowerCase(Locale.ROOT) + "\" \"" + secretpostgresadmin + "\" \"" + secretUsername + "\" \"" + secretPassword + "\"");
                        printWriter.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    secretpostgresadmin="";
                }
                if (!secretpostgresuser.equals("")){
                    PrintWriter printWriter;
                    try {
                        FileWriter fileWriter = new FileWriter(METHODS, true); //Set true for append mode
                        printWriter = new PrintWriter(fileWriter);
                        printWriter.println("# Name " + secretName);
                        //printWriter.println("# " + secretNotes);
                        printWriter.println("./do_pg.sh pg_user \"" + namespace.toLowerCase(Locale.ROOT) + "\" \"" + secretpostgresuser + "\" \"" + secretUsername + "\" \"" + secretPassword + "\"");
                        printWriter.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    secretpostgresuser="";
                }
                exit_loop = true;
        }
    } while(myReader.hasNextLine()&&!exit_loop);
}

    private static void get_namespace() { // Namespace ist speziell
        boolean exit_loop = false;
        namespace = data.replaceAll("(.*)<Name>", "");
        namespace = namespace.replaceAll("</Name>", "");
        data = myReader.nextLine();
        namespace_comment = data.replaceAll("(.*)<Notes>", "");
        if (!data.matches("(.*)<Notes/>")) { // Jetzt sind wir am Ende
            get_end("</Notes>");
        }
    }

    private static void get_group2() {
        boolean exit_loop = false;
        do {
            data = myReader.nextLine();
            if (data.matches("(.*)</Group>")) { // Jetzt sind wir am Ende
                exit_loop = true;
            }
            if (data.matches("(.*)<Name>(.*)</Name>")) {
                get_namespace();
            }
            if (data.matches("(.*)<Entry>")) {
                get_entry();
            }
        } while (myReader.hasNextLine() && !exit_loop);
    }

    private static void get_group1() {
        boolean exit_loop = false;
        do {
            data = myReader.nextLine();
            if (data.matches("(.*)</Group>")) { // Jetzt sind wir am Ende
                exit_loop = true;
            }
            if (data.matches("(.*)<Group>")) {
                get_group2();
            }
        } while (myReader.hasNextLine() && !exit_loop);
    }

    private static void get_root() {
        boolean exit_loop = false;
        File myObj = new File(FILENAME);

        try (PrintWriter writer = new PrintWriter(METHODS, StandardCharsets.UTF_8)) {
            writer.println("#/bin/bash");
            writer.println("# Methoden generiert am "+ LocalDateTime.now());
            writer.println("# Generiertes File um die Passwortänderungen durchzuführen");
        } catch (Exception e){
            e.printStackTrace();
        }                    secretUsernameraw = myval;


        try (PrintWriter writer = new PrintWriter(SECRETS, StandardCharsets.UTF_8)) {
            writer.println("---");
            writer.println("# Secrets generiert am "+ LocalDateTime.now());
        } catch (Exception e){
            e.printStackTrace();
        }

        try {
            myReader = new Scanner(myObj);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        do {
            data = myReader.nextLine();
            if (data.matches("(.*)</Root>")) { // Jetzt sind wir am Ende
                exit_loop = true;
            }
            if (data.matches("(.*)<Group>")) {
                get_group1();
            }
        } while (myReader.hasNextLine() && !exit_loop);
        myReader.close();
    }

    public static String getRandomFilename(String filePath, String extension) {
        String randomGuid = UUID.randomUUID().toString().replace("-", "");
        String directory = filePath;
        if (!directory.endsWith(File.separator)) {
            directory += File.separator;
        }
        if ((extension != null) && extension.startsWith(".") && (extension.length() > 1)) {
            extension = extension.substring(1);
        }
        return String.format("%s%s.%s", directory, randomGuid, extension);
    }

    static void printLinuxCommand(String command) throws Exception
    {
        //System.out.println("Linux command: " + command);
        String line;
        Process process = Runtime.getRuntime().exec(command);
        Reader r = new InputStreamReader(process.getInputStream());
        BufferedReader in = new BufferedReader(r);
        while((line = in.readLine()) != null) System.out.println(line);
        in.close();
        process.waitFor();
        //System.out.println("Exit Value: " + process.exitValue());
    }

    public static void main(String[] args)
    {
        Process process;
        try {
            // Temporäre Directories neu erzeugen
            printLinuxCommand("rm -fR /tmp/basic");
            printLinuxCommand("mkdir -p /tmp/basic");
            get_root();
            // Basic Secrets erzeugen
            File dir = new File("/tmp/basic");
            File[] directoryListing = dir.listFiles();
            if (directoryListing != null) {
                for (File child : directoryListing) {
                    String filename = getRandomFilename("/tmp","tmp");
                    String content = Files.readString(child.toPath(), Charset.defaultCharset());
                    //System.out.println(content);
                    content = Base64.getEncoder().encodeToString(content.getBytes());
                    //System.out.println(content);
                    try {
                        FileWriter fileWriter = new FileWriter(filename, false); //Set true for append mode
                        PrintWriter printWriter = new PrintWriter(fileWriter);
                        printWriter.println("#!/bin/bash");
                        printWriter.println("shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.");
                        printWriter.println("shopt -o -s xtrace #—Displays each command before it’s executed.");
                        printWriter.println("shopt -o -s nounset #-No Variables without definition");
                        printWriter.println("sed -i 's,  auth:.*,  auth: "+content+",g' "+BAUTH+"/"+child.getName()+".yaml ");
                        printWriter.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    printLinuxCommand("chmod 755 "+filename );
                    printLinuxCommand("/bin/bash "+filename);
                    printLinuxCommand("rm -f "+filename);
                }
            }
            printLinuxCommand("rm -fR /tmp/basic");
//            process = Runtime.getRuntime().exec("mkdir -p /tmp/basic");
//              kubectl create secret generic basic-auth --from-file=auth
            //              htpasswd -cb auth alfred dasisteinlangespasswortfürmich
//              htpasswd -b auth susi franz

            // Temporäre Directories löschen
  //          process = Runtime.getRuntime().exec("rmdir --ignore-fail-on-non-empty -p /tmp/basic");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}