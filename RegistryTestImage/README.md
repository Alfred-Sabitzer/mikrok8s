# Special Image

Diese Software dient zum Testen von Push und Pulls in/aus einer lokalen, verschlüsselten, privaten Registry im K8S-Cluster

# Test der Erreichbarkeit der Registry im K8S-Cluster

Zuerst müssen die richtigen Settings eingestellt sein.

````
docker_registry="docker.k8s.slainte.at"
docker_username=$(cat ${HOME}/.password/docker_username.txt)
docker_password=$(cat ${HOME}/.password/docker_password.txt)
````
Danach kann man sich zur Registry verbinden

````
curl -u ${docker_username}:${docker_password} -k -v https://${docker_registry}/v2/_catalog 
````
Das Ergebnis sollte dann in etwa so aussehen:

````
*   Trying 87.243.186.152:443...
* Connected to docker.k8s.slainte.at (87.243.186.152) port 443 (#0)
* ALPN, offering h2
* ALPN, offering http/1.1
* TLSv1.3 (OUT), TLS handshake, Client hello (1):
* TLSv1.3 (IN), TLS handshake, Server hello (2):
* TLSv1.3 (IN), TLS handshake, Encrypted Extensions (8):
* TLSv1.3 (IN), TLS handshake, Certificate (11):
* TLSv1.3 (IN), TLS handshake, CERT verify (15):
* TLSv1.3 (IN), TLS handshake, Finished (20):
* TLSv1.3 (OUT), TLS change cipher, Change cipher spec (1):
* TLSv1.3 (OUT), TLS handshake, Finished (20):
* SSL connection using TLSv1.3 / TLS_AES_256_GCM_SHA384
* ALPN, server accepted to use h2
* Server certificate:
*  subject: CN=docker.k8s.slainte.at
*  start date: Apr  9 18:15:59 2022 GMT
*  expire date: Jul  8 18:15:58 2022 GMT
*  issuer: C=US; O=Let's Encrypt; CN=R3
*  SSL certificate verify result: unable to get local issuer certificate (20), continuing anyway.
* Using HTTP2, server supports multiplexing
* Copying HTTP/2 data in stream buffer to connection buffer after upgrade: len=0
* Server auth using Basic with user 'docker'
* h2h3 [:method: GET]
* h2h3 [:path: /v2/_catalog]
* h2h3 [:scheme: https]
* h2h3 [:authority: docker.k8s.slainte.at]
* h2h3 [authorization: Basic ZG9ja2VyOkV2N2F3b2dBb3REaW9FSE96Y2lQdkpIRA==]
* h2h3 [user-agent: curl/7.82.0]
* h2h3 [accept: */*]
* Using Stream ID: 1 (easy handle 0x559d3f29dd70)
> GET /v2/_catalog HTTP/2
> Host: docker.k8s.slainte.at
> authorization: Basic ZG9ja2VyOkV2N2F3b2dBb3REaW9FSE96Y2lQdkpIRA==
> user-agent: curl/7.82.0
> accept: */*
> 
* TLSv1.3 (IN), TLS handshake, Newsession Ticket (4):
* TLSv1.3 (IN), TLS handshake, Newsession Ticket (4):
* old SSL session ID is stale, removing
* Connection state changed (MAX_CONCURRENT_STREAMS == 128)!
< HTTP/2 200 
< date: Sat, 23 Apr 2022 07:59:28 GMT
< content-type: application/json; charset=utf-8
< content-length: 20
< access-control-allow-credentials: true
< access-control-allow-headers: Authorization
< access-control-allow-headers: Accept
< access-control-allow-methods: HEAD
< access-control-allow-methods: GET
< access-control-allow-methods: OPTIONS
< access-control-allow-methods: DELETE
< access-control-allow-origin: http://localhost:443
< access-control-expose-headers: Docker-Content-Digest
< access-control-max-age: 1728000
< docker-distribution-api-version: registry/2.0
< x-content-type-options: nosniff
< strict-transport-security: max-age=15724800; includeSubDomains
< 
{"repositories":[]}
* Connection #0 to host docker.k8s.slainte.at left intact
````

