#!/bin/bash
############################################################################################
#
# Bauen der Sourcen
# vorher einloggen mit docker login https://docker.k8s.slainte.at -u docker -p <password>
#
############################################################################################
docker_registry="docker.k8s.slainte.at"
docker_username=$(cat ${HOME}/.password/docker_username.txt)
docker_password=$(cat ${HOME}/.password/docker_password.txt)
docker login ${docker_registry} -u ${docker_username} -p ${docker_password}
git pull
cd ./ci.docker.raspberrypi-master/kernel/
./do.sh
cd ../..
#
src/build-services.sh 1.16.3 ${docker_registry}

