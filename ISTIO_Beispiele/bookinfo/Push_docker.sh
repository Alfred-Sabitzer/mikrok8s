#!/bin/bash
############################################################################################
#
# pushen der Sourcen in das Repo
# vorher einloggen mit docker login https://${docker_registry} -u docker -p <password>
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
docker_registry="docker.k8s.slainte.at"
docker_username=$(cat ${HOME}/.password/docker_username.txt)
docker_password=$(cat ${HOME}/.password/docker_password.txt)
docker login ${docker_registry} -u ${docker_username} -p ${docker_password}
docker push ${docker_registry}/examples-bookinfo-mongodb:1.16.3
docker push ${docker_registry}/examples-bookinfo-mongodb:latest
docker push ${docker_registry}/examples-bookinfo-mysqldb:1.16.3
docker push ${docker_registry}/examples-bookinfo-mysqldb:latest
docker push ${docker_registry}/examples-bookinfo-ratings-v-unhealthy:1.16.3
docker push ${docker_registry}/examples-bookinfo-ratings-v-unhealthy:latest
docker push ${docker_registry}/examples-bookinfo-ratings-v-unavailable:1.16.3
docker push ${docker_registry}/examples-bookinfo-ratings-v-unavailable:latest
docker push ${docker_registry}/examples-bookinfo-ratings-v-delayed:1.16.3
docker push ${docker_registry}/examples-bookinfo-ratings-v-delayed:latest
docker push ${docker_registry}/examples-bookinfo-ratings-v-faulty:1.16.3
docker push ${docker_registry}/examples-bookinfo-ratings-v-faulty:latest
docker push ${docker_registry}/examples-bookinfo-ratings-v2:1.16.3
docker push ${docker_registry}/examples-bookinfo-ratings-v2:latest
docker push ${docker_registry}/examples-bookinfo-ratings-v1:1.16.3
docker push ${docker_registry}/examples-bookinfo-ratings-v1:latest
docker push ${docker_registry}/examples-bookinfo-reviews-v3:1.16.3
docker push ${docker_registry}/examples-bookinfo-reviews-v3:latest
docker push ${docker_registry}/examples-bookinfo-reviews-v2:1.16.3
docker push ${docker_registry}/examples-bookinfo-reviews-v2:latest
docker push ${docker_registry}/examples-bookinfo-reviews-v1:1.16.3
docker push ${docker_registry}/examples-bookinfo-reviews-v1:latest
docker push ${docker_registry}/examples-bookinfo-details-v2:1.16.3
docker push ${docker_registry}/examples-bookinfo-details-v2:latest
docker push ${docker_registry}/examples-bookinfo-details-v1:1.16.3
docker push ${docker_registry}/examples-bookinfo-details-v1:latest
docker push ${docker_registry}/examples-bookinfo-productpage-v-flooding:1.16.3
docker push ${docker_registry}/examples-bookinfo-productpage-v-flooding:latest
docker push ${docker_registry}/examples-bookinfo-productpage-v1:1.16.3
docker push ${docker_registry}/examples-bookinfo-productpage-v1:latest
docker push ${docker_registry}/websphere-liberty:kernel

