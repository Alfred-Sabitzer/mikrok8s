#!/bin/bash
############################################################################################
# Bauen und deployen
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
docker_registry="docker.k8s.slainte.at"
docker_username=$(cat ${HOME}/.password/docker_username.txt)
docker_password=$(cat ${HOME}/.password/docker_password.txt)
git pull
docker build --no-cache --force-rm . -t ${docker_registry}/websphere-liberty:kernel
docker push ${docker_registry}/websphere-liberty:kernel
curl -u ${docker_username}:${docker_password} -k https://${docker_registry}/v2/websphere-liberty/tags/list
#