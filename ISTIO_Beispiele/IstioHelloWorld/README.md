# Istio Hello World

Das ist ein Test-Image, um zu überprüfen, ob die Istio richtig funktioniert (Sidecars werden erzeugt usw.).
## Build docker images

```bash
./do.sh
```
Danach muß der Service deployed werden.

```bash
$ kubectl apply -f .
```
Jetzt ist der Service im Cluster verfügbar und erreichbar.
