# Istio

Hier werden die lokalen ISTIO-Beispiele gebaut und deployed

Jedes Verzeichnis hat seine eigene Anleitung.

# Achtung - Das funktioniert nicht

Es ist recht schwierig, ISTIO-Projekte zu finden, die am Raspberry-PI funktionieren. Zum Zeitpunkt der Erstellung, war die nur
docker.io/querycapistio/. Großer Dank an den Entwickler.

Ich habe noch ein paar lokale Adaptionen dazugefügt. Prinzipeill kommt ISTI hoch, es ist mir aber nicht gelungen Services sinnvoll zu deployen. Darum habe ich diese Versuche eingestellt (und warte auf ein offiziell supportetes ISTIO-Feature in microK8s)-
