#!/bin/sh
#############################################################################################"
#"
# Erzeugen der configmaps"
#"
#############################################################################################"
#'/home/alfred/gitlab/k8sapp/dev/make_configmap_dir.sh' ./config ./PostgresDB_config.yaml
#'/home/alfred/gitlab/k8sapp/dev/make_configmap_dir.sh' ./init ./PostgresDB_init.yaml
for f in *.yaml
do
  sed -i -r 's/#namespace#/slainte/g' ${f}
done
