#!/bin/sh
export POSTGRES_PASSWORD=$(cat /pgadmin/password)
export POSTGRES_USER=$(cat /pgadmin/username)
export POSTGRES_DB=$(cat /pgadmin/username)
export PGUSER=$(cat /pgadmin/username)
export PGPASSWORD=$(cat /pgadmin/password)
file=$(mktemp /tmp/do_pg.XXXXXX)
cat <<XXX > ${file}
-- check DB-Connect
-- Das liefert einen Fehler ERROR:  statement returning results not allowed
-- Zeigt uns aber, dass der DB-Link prinzipiell geht.
SELECT dblink_exec('dbname=' || current_database() , 'select 1;', true);
-- Anlegen des Users
select create_user('TestUser','MeinSuperGeheimesPasswort');
select set_password('TestUser','MeinSuperGeheimesPasswort');
-- Anlegen der Datenbank
-- SELECT dblink_exec('dbname=' || current_database() , 'CREATE DATABASE TestUser WITH OWNER TestUser', true);
select create_database('TestUser','TestUser');
GRANT ALL ON DATABASE "TestUser" TO "TestUser";
XXX
psql -e -f ${file}
rm  ${file}