#!/bin/bash
############################################################################################
#
# Execute Skript in pod
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
kubectl exec -i --namespace=slainte postgresdb-state-0 -c postgresdb -- sh < test_exec.txt
