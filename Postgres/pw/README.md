# Passwort-Management

Mit diesen Hilfsmitteln können, die im keepassxc definierten Passworte auch in der Postgres-Datenbank angewendet werden.  

# Ablauf

Die Passworte und Rollenzuordnungen werden im keepassxc gepflegt. Mit ReadXML wird dieser Inhalt extrahiert. Es werden die Secrets erzeugt, und eben auch das Steuerskript für das Verwalten im Postgres.

```bash
./mykeepassmethods.sh
```
Dieses Skript wird automatisch erzeugt (Siehe auch ReadXML).

Anhand der Rollen im Keepass

* pg_admin (für Super-User, die auch eine Datenbank bekommen) 
* pg_user (für Datenbankuser aka Profile, die sich anmelden können, aber keine eigene Datenbank besitzen)
 
werden verschiedene Profile erzeugt. Diese Informationen werden im Skript do_pg.sh an die Datenbank übermittelt.

Nach dem Einspielen der Passworte und Rollen in die Datenbank müssen auch die Secrets an Kubernetes übermittelt werden.
Danach muß man die betrofffenen Services Restarten.

# Beispiel NextCloudPostgres
./do_pg.sh pg_admin "slainte" "postgresdb-svc" "bmV4dGNsb3Vk" "Zk0wNnh3a2ZiVmoyS0pYNzJ6d2hOT2tudjBneFlD"

Im Namespace slainte am Service postgresdb-svc hängt ein Pod (Service-Endpoint). In diesem Pod gibt es einen Container der (und das ist eine Konvention) **postgresdb** heisst.
Zu diesem Container wird eine Verbindung aufgebaut, und dann wird folgendes Skript ausgeführt.

```psql
-- Anlegen des Users
select create_superuser('${3}','${4}');
select set_password('${3}','${4}');
-- Anlegen der Datenbank
select create_database('${3}','${3}');
GRANT ALL ON DATABASE "${3}" TO "${3}";
```
Das ist das Beispiel für die Anlage eines Schema-Users. 

