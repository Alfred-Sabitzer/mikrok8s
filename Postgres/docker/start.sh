#!/bin/sh
echo "############################################################################################"
echo "#"
echo "# Starten der Postgres mit den richtigen Werten."
echo "#"
echo "############################################################################################"
#
#ls -lisa /
ls -lisa /docker-entrypoint-initdb.d/*.*
#
echo "############################################################################################"
echo "#"
echo "# Kopieren der TLS-Keys."
echo "#"
echo "############################################################################################"
#
if [ ! -d "/postgres/TLS/" ]; then
  echo "Erzeuge das Directory für die Zertifikate"
  mkdir –m 777 –p /postgres/TLS/
fi
for f in /certificate/*
do
        echo "Processing $f"
        cp -v $f /postgres/TLS/
done
for f in /postgres/TLS/*
do
        echo "Processing $f"
        chown postgres:root $f
        chmod 0600 $f
done
ls -lisa /postgres/TLS/*
# Passwort im Hintergrund
/set_password.sh &
#
echo "############################################################################################"
echo "#"
echo "# Starten der Datenbank."
echo "#"
echo "############################################################################################"
#
docker-entrypoint.sh postgres
#/dummy.sh