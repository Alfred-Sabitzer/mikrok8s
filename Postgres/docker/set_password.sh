#!/bin/sh
############################################################################################
#
# Setzen des Passwortes für den Admin (sicher ist sicher)
#
############################################################################################
echo # Wir warten 5 Minuten
sleep 360
#
file=$(mktemp /tmp/do_pg.XXXXXX)
cat <<XXX > ${file}
select set_password('${POSTGRES_USER}','${POSTGRES_PASSWORD}');
XXX
#cat $file
psql -e -f ${file}
rm  ${file}
echo "############################################################################################"
echo "#"
echo "# Passwort für ${POSTGRES_USER} wurde gesetzt"
echo "#"
echo "############################################################################################"
