#!/bin/sh
echo "############################################################################################"
echo "#"
echo "# Anlage des Superusers "
echo "# Manche alten Programme gehen davon aus, dass es einen postgres-user gibt "
echo "# Warum auch immer:  [208] FATAL:  role "root" does not exist "
echo "#"
echo "############################################################################################"
#
whoami
#
cat <<EOF > /tmp/create_user.sql

select create_superuser('${POSTGRES_USER}','${POSTGRES_PASSWORD}');
select set_password('${POSTGRES_USER}','${POSTGRES_PASSWORD}');
select create_database('${POSTGRES_USER}','${POSTGRES_USER}');
GRANT ALL ON DATABASE "${POSTGRES_USER}" TO "${POSTGRES_USER}";

select create_superuser('postgres','${POSTGRES_PASSWORD}');
select set_password('postgres','${POSTGRES_PASSWORD}');
select create_database('postgres','postgres');
GRANT ALL ON DATABASE "postgres" TO "postgres";

select create_superuser('root','${POSTGRES_PASSWORD}');
select set_password('root','${POSTGRES_PASSWORD}');
select create_database('root','root');
GRANT ALL ON DATABASE "root" TO "root";

select * from pg_catalog.pg_database;
select * from pg_catalog.pg_user;

SELECT *
FROM pg_catalog.pg_tables
WHERE schemaname != 'pg_catalog' AND
    schemaname != 'information_schema';

EOF
psql -e -f /tmp/create_user.sql
rm  /tmp/create_user.sql
#
echo "############################################################################################"
echo "#"
echo "# Superuser done"
echo "#"
echo "############################################################################################"
#