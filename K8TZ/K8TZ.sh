#!/bin/bash
############################################################################################
#
# MicroK8S Installieren von K8TZ um die Zeit in den Pods zu synchronisieren
# https://github.com/k8tz/k8tz
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
indir=$(dirname "$0")
# K8TZ Disablen
ansible pc1 -m shell -a 'microk8s.helm3 repo add k8tz https://k8tz.github.io/k8tz/'
ansible pc1 -m shell -a 'microk8s.helm3 repo update '
ansible pc1 -m shell -a 'microk8s.helm3 delete k8tz '
# K8TZ Enablen
ansible pc1 -m shell -a 'microk8s.helm3 install k8tz k8tz/k8tz \
  --set timezone=Europe/Vienna \
  --set injectionStrategy=initContainer \
  --set injectAll=false '
#
# Jetzt haben alle Pods diesselbe Zeitzone
#
