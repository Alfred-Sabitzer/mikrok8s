# K8TZ

K8TZ ist ein nettes Beispiel für eine Software, die Sidecars hinzufügt.

# Installation

```bash
./K8TZ.sh
```
Damit wird K8TZ installiert, und der Servide steht zur Verfügung.

# Achtung

K8TZ funktioniert derzeit nicht bei privileged Services (wie zb. den nginx-Ingressen). Weiters wird die Option "k8tz.io/inject 	Decide whether k8tz should inject timezone or not" nicht unterstützt (bzw. ignoriert).
