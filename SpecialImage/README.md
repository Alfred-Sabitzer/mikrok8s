# Special Image

Diese Software dient zum Testen von Push/Pulls in/aus einer lokalen, verschlüsselten, privaten Registry.

# Test der Erreichbarkeit der lokalen Registry

Dieses Spezialimage ist ein einfache alpine. Es dient zum Testen der Registry.
Zuerst müssen die richtigen Setting eingestellt sein.

````
docker_registry="monitoring.slainte.at:5000"
docker_username=$(cat ${HOME}/.password/docker_basis_username.txt)
docker_password=$(cat ${HOME}/.password/docker_basis_password.txt)
````
Danach kann man sich zur Registry verbinden

````
curl -u ${docker_username}:${docker_password} -k -v https://${docker_registry}/v2/_catalog 
````
Das Ergebnis sollte dann in etwa so aussehen:

````
* 
* Trying 192.168.0.2:5000...
* Connected to monitoring.slainte.at (192.168.0.2) port 5000 (#0)
* ALPN, offering h2
* ALPN, offering http/1.1
* TLSv1.3 (OUT), TLS handshake, Client hello (1):
* TLSv1.3 (IN), TLS handshake, Server hello (2):
* TLSv1.3 (IN), TLS handshake, Encrypted Extensions (8):
* TLSv1.3 (IN), TLS handshake, Certificate (11):
* TLSv1.3 (IN), TLS handshake, CERT verify (15):
* TLSv1.3 (IN), TLS handshake, Finished (20):
* TLSv1.3 (OUT), TLS change cipher, Change cipher spec (1):
* TLSv1.3 (OUT), TLS handshake, Finished (20):
* SSL connection using TLSv1.3 / TLS_CHACHA20_POLY1305_SHA256
* ALPN, server accepted to use h2
* Server certificate:
* subject: CN=monitoring.slainte.at
* start date: Apr  1 13:05:36 2022 GMT
* expire date: Jun 30 13:05:35 2022 GMT
* issuer: C=US; O=Let's Encrypt; CN=R3
* SSL certificate verify result: unable to get local issuer certificate (20), continuing anyway.
* Using HTTP2, server supports multiplexing
* Copying HTTP/2 data in stream buffer to connection buffer after upgrade: len=0
* Server auth using Basic with user 'dockerbasis'
* h2h3 [:method: GET]
* h2h3 [:path: /v2/_catalog]
* h2h3 [:scheme: https]
* h2h3 [:authority: monitoring.slainte.at:5000]
* h2h3 [authorization: Basic ZG9ja2VyYmFzaXM6bVozWEFJT0hXM0tPVTJHekhnUGtGYndL]
* h2h3 [user-agent: curl/7.82.0]
* h2h3 [accept: */*]
* Using Stream ID: 1 (easy handle 0x563a1635cd70)
> GET /v2/_catalog HTTP/2
> Host: monitoring.slainte.at:5000
> authorization: Basic ZG9ja2VyYmFzaXM6bVozWEFJT0hXM0tPVTJHekhnUGtGYndL
> user-agent: curl/7.82.0
> accept: */*
> 
* TLSv1.3 (IN), TLS handshake, Newsession Ticket (4):
* Connection state changed (MAX_CONCURRENT_STREAMS == 250)!
< HTTP/2 200 
< access-control-allow-credentials: true
< access-control-allow-headers: Authorization
< access-control-allow-headers: Accept
< access-control-allow-methods: HEAD
< access-control-allow-methods: GET
< access-control-allow-methods: OPTIONS
< access-control-allow-methods: DELETE
< access-control-allow-origin: https://monitoring.slainte.at:8123
< access-control-expose-headers: Docker-Content-Digest
< access-control-max-age: 1728000
< content-type: application/json; charset=utf-8
< docker-distribution-api-version: registry/2.0
< x-content-type-options: nosniff
< content-length: 79
< date: Sat, 23 Apr 2022 07:54:46 GMT
< 
{"repositories":["registry-ui","registry-uik8s","registryk8s","specialimage"]}
* Connection #0 to host monitoring.slainte.at left intact
````

