# K8S Servicemesh Linkerd mit MicroK8S auf Raspberry PI

Beispiele für das Entwickeln und Betreiben von Anwendungen mit einem
Servicemesh auf einem mit einer Firewall gesicherten MicroK8s Cluster auf
Raspberry PI Basis.

## Erklärungen

Leider ist nicht alles am Raspberry mit arm-Architekture verfügbar. Somit kann ein K8S-Cluster auf
Raspberry Basis nicht wirklich mit einem Cluster auf Intel-Basis (oder noch besser in einer
wirklichen Cloud wie Google oder Amazon) verglichen werden. Es ist dennoch eine gute und
kostengünstige Basis um die Prinzipien kennen zu lernen, und Hands-On einen Cluster vom Scratch
aufzubauen.
Der hier beschriebene Cluster baut auf den Büchern

* "Bau einer K8s bare-metal-cloud mit RaspberryPI" ISBN 978-3-7534-9851-4
* "K8s Applications mit MicroK8S auf Raspberry PI" ISBN 978-3-7427-7013-4
 
auf.

## Aktuelles EBook

Das aktuelle Ebook ist

"K8S Servicemesh Linkerd mit MicroK8S auf Raspberry PI" ISBN 978-3-7549-8554-0. 