#!/bin/bash
############################################################################################
# Erzeugen einer Secure Registry mit Docker-compose auf dem Build-Rechner
# Diese Registry dient zum Speichern von Basisimages, die der Cluster zum starten braucht
#
# https://docs.docker.com/registry/deploying/
#
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
git pull

docker_registry="monitoring.slainte.at:5000"
docker_username=$(cat ${HOME}/.password/docker_basis_username.txt)
docker_password=$(cat ${HOME}/.password/docker_basis_password.txt)
tag=$(date +"%Y%m%d")

docker-compose -f simple.yml down

cd ./registry
./do.sh ${tag}

cd ..
cd ./registry-ui
./do.sh ${tag}
cd ..

docker-compose -f simple.yml up -d

sleep 5
curl -u ${docker_username}:${docker_password} -k https://${docker_registry}/v2/_catalog           # Check ob man was zurückbekommt

./populate.sh ${tag}
#
