#!/bin/bash
############################################################################################
# Bauen und deployen
# Auch dieses Image kommt in die Basisregistry
############################################################################################
#shopt -o -s errexit    #—Terminates  the shell script  if a command returns an error code.
#shopt -o -s xtrace #—Displays each command before it’s executed.
shopt -o -s nounset #-No Variables without definition
docker_registry="monitoring.slainte.at:5000"
git pull --ff-only
image="registry-uik8s"
tag=${1}

docker build --no-cache --force-rm . -t ${docker_registry}/${image}:${tag} -t ${docker_registry}/${image}:latest
#
# Jetzt ist die UI lokal vorhanden
#
