# Docker Registry als Static-Beispiel

Diese Registry-UI ist von aussen sichtbar. In diesem Falle https://monitoring.slainte.at:8123.
Die Registry dahinter ist unter https://monitoring.slainte.at:5000 erreicbar. Beide benutzen diesselben keys 
(kommen vom Server und werden über letsencrypt bereitgestellt).

Die Konfiguration is im `simple.yml` enthaltem. Das Beispiel basiert auf https://github.com/Joxit/docker-registry-ui/tree/main/examples/ui-as-standalone 
und wurde für die lokalen Bedürfnisse angepasst.

Der Bau der Images und das befüllen der Registry erfolgt mit

```sh
./dc_secure_registry.sh
```

Das ist ein Docker-Compose-Beispiel.

# Zweck dieser Registry

Diese Registry enthält alle Basis-Images, und dient dem Kubernetes-Cluster um
seine Services starten zu können (z.b. hat der Kubernetes Cluster auch eine
Registry, in der dann die fachlichen Services abgelegt sind. Diese Registry
im Kubernetes-Cluster lädt das Image aus der Starter-Registry). Das vermeidet
unnötiges laden aus dem Internet. Ausserdem bleiben die Images stabil, und
können kontrolliert auf neuere Versionen upgedated werden. Ein direktes laden
aus dem Internet hat u.U. Nebeneffekte zur Folge.

# Test der Registry

Die prinzipielle Erreichbarkeit und Verfügbarkeit kann mit curl getestet werden.

```sh
docker_registry="monitoring.slainte.at"
docker_username=$(cat ${HOME}/.password/docker_basis_username.txt)
docker_password=$(cat ${HOME}/.password/docker_basis_password.txt)
curl -u ${docker_username}:${docker_password}  -k -v https://${docker_registry}:5000/v2/_catalog 
curl -u ${docker_username}:${docker_password}  -k -v https://${docker_registry}:8123 
```
Beide Abfragen sollten vernünftige Ergebnisse liefern.

# Einspielen der Passworte

Damit die Secure-Registry abgefragt werden kann, muß das richtige Passwort in jedem Namespace vorhanden sein.
Dazu wird das dockerbasicjson Password verwendet, das sich bereits im Namespace admin befindet.

```sh
./create_dockerbasicjson.sh 
secret "dockerbasicjson" deleted
secret/dockerbasicjson created
kube-system
kube-public
kube-node-lease
metallb-system
ingress
cert-manager
longhorn-system
monitoring
linkerd
linkerd-viz
slainte
default
admin
secret "dockerbasicjson" deleted
secret "dockerbasicjson" deleted
secret "dockerbasicjson" deleted
secret "dockerbasicjson" deleted
secret "dockerbasicjson" deleted
secret "dockerbasicjson" deleted
secret "dockerbasicjson" deleted
secret "dockerbasicjson" deleted
secret "dockerbasicjson" deleted
secret "dockerbasicjson" deleted
secret "dockerbasicjson" deleted
Error from server (NotFound): error when deleting "dockerbasicjson.yaml": secrets "dockerbasicjson" not found
Error from server (NotFound): error when deleting "dockerbasicjson.yaml": secrets "dockerbasicjson" not found
secret/dockerbasicjson created
secret/dockerbasicjson created
secret/dockerbasicjson created
secret/dockerbasicjson created
secret/dockerbasicjson created
secret/dockerbasicjson created
secret/dockerbasicjson created
secret/dockerbasicjson created
secret/dockerbasicjson created
secret/dockerbasicjson created
secret/dockerbasicjson created
secret/dockerbasicjson created
secret/dockerbasicjson created
```

# Generieren der Basic-Auth Zugänge

Damit man sich an der Registry anmelden kann, braucht es Credentials.
Das Skript befindet sich im Verzeichnis pem

```sh
./create_htpasswd.sh
Adding password for user dockerbasis
Adding password for user docker
```

# Generieren der Zertifikate

Die Zertifikate werden als trusted generiert, und können lokal am Docker verwendet werden. Im K8S werden üer das yaml allerdings die Zertifiakte dses Zertifikatsmanagers verwendet.

```sh
$ ./generate_certificates.sh 
Certificate request self-signature ok
subject=C = CA, ST = QC, O = Slainte, CN = slainte - Server-only
Certificate request self-signature ok
subject=C = CA, ST = QC, O = Slainte, CN = slainte - Client-only
Certificate request self-signature ok
subject=C = CA, ST = QC, O = Slainte, CN = slainte - Generic-cert
Generating DH parameters, 1024 bit long safe prime
................................................................................................................................................................................................................................................................................................+....+.................................................................................................................................................................................+.........................................................................................+.........................................++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*++*
Alle Zertifikate generiert
insgesamt 52
  394858 4 drwxrwxr-x 2 alfred alfred 4096 Okt 20 18:21 .
19398736 4 drwx-----T 7 alfred alfred 4096 Okt 20 18:15 ..
  394947 4 -rw-rw-r-- 1 alfred alfred 1960 Okt 20 18:21 ca.crt
  394936 4 -rw------- 1 alfred alfred 3272 Okt 20 18:21 ca.key
  394952 4 -rw-rw-r-- 1 alfred alfred 1996 Okt 20 18:21 client.crt
  394951 4 -rw------- 1 alfred alfred 3272 Okt 20 18:21 client.key
  394946 4 -rwxrwxr-x 1 alfred alfred 2526 Okt 20 18:20 generate_certificates.sh
  394948 4 -rw-rw-r-- 1 alfred alfred  162 Okt 20 18:21 openssl.cnf
  394954 4 -rw-rw-r-- 1 alfred alfred 1854 Okt 20 18:21 redis.crt
  394955 4 -rw-rw-r-- 1 alfred alfred  245 Okt 20 18:21 redis.dh
  394953 4 -rw------- 1 alfred alfred 3272 Okt 20 18:21 redis.key
  394950 4 -rw-rw-r-- 1 alfred alfred 1996 Okt 20 18:21 server.crt
  394949 4 -rw------- 1 alfred alfred 3272 Okt 20 18:21 server.key
```
